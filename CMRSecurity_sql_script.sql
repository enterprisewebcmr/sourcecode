﻿USE [CMRSecurity]
GO

/****** Object:  Table [dbo].[AccountRoles]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccountRoles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [nvarchar](255) NOT NULL,
	[AccountID] [int] NOT NULL,
	[RoleName] [nvarchar](512) NOT NULL,
	[RoleID] [int] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Accounts]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Accounts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[Email] [nvarchar](255) NULL,
	[Phone] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Locked] [bit] NOT NULL,
	[SupperAdmin] [bit] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[MenuPages]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MenuPages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [nvarchar](255) NOT NULL,
	[MenuID] [int] NOT NULL,
	[PageName] [nvarchar](255) NOT NULL,
	[PageURL] [nvarchar](255) NULL,
	[PageID] [int] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Menus]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Menus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [nvarchar](255) NOT NULL,
	[CssClass] [nvarchar](255) NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[PagePermissionRoles]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PagePermissionRoles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FuncName] [nvarchar](512) NOT NULL,
	[PageID] [int] NOT NULL,
	[PagePermissionID] [int] NOT NULL,
	[RoleName] [nvarchar](512) NOT NULL,
	[RoleID] [int] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[PagePermissions]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PagePermissions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PageID] [int] NOT NULL,
	[PageURL] [nvarchar](512) NOT NULL,
	[FuncName] [nvarchar](512) NOT NULL,
	[Description] [nvarchar](512) NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[PageRoles]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PageRoles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](512) NOT NULL,
	[PageID] [int] NOT NULL,
	[RoleName] [nvarchar](512) NOT NULL,
	[RoleID] [int] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Pages]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Pages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](512) NOT NULL,
	[PageURL] [nvarchar](512) NULL,
	[ParentPageID] [int] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Roles]    Script Date: 2/15/2016 8:43:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](512) NOT NULL
) ON [PRIMARY]

GO


