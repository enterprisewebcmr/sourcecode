﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Models;
using BackOffices.Dal;

namespace BackOffices.Controllers
{
    public class StudentController : Controller
    {
        //
        // GET: /Student/
        CMRSecurityEntities cmrs = new CMRSecurityEntities();
        int PageSize = 5;
        public ActionResult Index()
        {
            List<BackOffices.Models.Students> lst = new List<Students>();
            lst = GetAllStudent();

            return RedirectToAction("ManageStudentList", "Student", new { PageNum = 1 });
        }
        public int countAll()
        {
            var query = from data in cmrs.Students
                        select data;
            return query.Count();
        }
        public ActionResult ManageStudentList(int pagenum)
        {
            //List<BackOffices.Models.Students> lst = new List<Students>();
            //lst = GetAllStudent();
            ViewBag.countnewsall = countAll();
            return View("Index", pading(PageSize, pagenum));
        }
        public List<Students> pading(int pagesize, int pagenum)
        {
            var lst = GetAllStudent(); // pagesize =5        
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            //gỉa sử có 6 bản ghi
            // pagesize =5  nếu mà pagenum=1 trang thứ 1 thì list=lst.skip(5*) take(5))
            //
            return list;
        }
        public List<Students> GetAllStudent()
        {
            DateTime.Now.ToString("dd/MM/yyyy");

            List<Students> lst = new List<Students>();
            var students = from data in cmrs.Students
                           orderby data.Create_Time descending
                           select new
                           {
                               data.Student_ID,
                               data.StudentName,
                               data.Status,
                               data.Phone_No,
                               data.Email,
                               data.DOB,
                               data.Address,

                           };

            foreach (var item in students)
            {
                Students stu = new Students();

                stu.Student_ID = item.Student_ID;
                stu.StudentName = item.StudentName;
                stu.Status = item.Status;
                stu.Phone_No = item.Phone_No;
                stu.Email = item.Email;
                stu.DOB = item.DOB.Value.ToString("dd/MM/yyyy");
                // stu.DOB = item.DOB;
                stu.Address = item.Address;
                lst.Add(stu);
            }

            return lst;
        }
        public ActionResult DeleteStudent(int id)
        {
            var student = (from data in cmrs.Students where data.Student_ID == id select data).FirstOrDefault();
            try
            {
                cmrs.Students.Remove(student);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "Error!";
                throw new Exception(ex.Message);
            }
            return RedirectToAction("Index", "Student");
        }

        public ActionResult GoToAdd()
        {
            return View("AddStudent");
        }
        [HttpPost]
        public ActionResult AddStudent(FormCollection collection)
        {
            string srtName = collection["Name"].Trim();
            string srtDOB = collection["Dob"].Trim();
            string srtPhoneno = collection["Phoneno"].Trim();
            string srtAddress = collection["Address"].Trim();
            string srtMail = collection["Email"].Trim();
            string srtStatus = collection["Status"].Trim();

            int status = Int32.Parse(srtStatus.Trim());
            Dal.Student student = new Student();
            student.StudentName = srtName;
            student.Status = status;
            student.Phone_No = srtPhoneno;
            student.Email = srtMail;
            student.Address = srtAddress;
            student.Create_Time = DateTime.Now;
            DateTime Date = Convert.ToDateTime(srtDOB.Trim());
            student.DOB = Date;
            var stu = (from data in cmrs.Students where data.Phone_No.Equals(srtPhoneno) select data).FirstOrDefault();
            if (stu != null)
            {
                TempData["MessageError"] = "Phone number is exist!";
                return View("AddStudent");
            }
            var st = (from data in cmrs.Students where data.Email.Equals(srtMail) select data).FirstOrDefault();
            if (st != null)
            {
                TempData["MessageError"] = "Email is exist!";
                return View("AddStudent");
            }

            try
            {
                cmrs.Students.Add(student);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
                return RedirectToAction("Index", "Student");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public ActionResult EditStudent(int id)
        {
            var student = (from data in cmrs.Students where data.Student_ID == id select data
                //new {
                // Student_ID= data.Student_ID,
                // StudentName= data.StudentName,
                // Status= data.Status,
                // Phone_No= data.Phone_No,
                // Email= data.Email,
                //DOB=  data.DOB,
                //Address=  data.Address,
                //}
            ).FirstOrDefault();
            //Students stu = new Students();

            //stu.Student_ID = student.Student_ID;
            //stu.StudentName = student.StudentName;
            //stu.Status = student.Status;
            //stu.Phone_No = student.Phone_No;
            //stu.Email = student.Email;
            //stu.DOB = student.DOB.Value.ToString("dd/MM/yyyy");
            //// stu.DOB = item.DOB;
            //stu.Address = student.Address;
            return View("EditStudent", student);
        }
        [HttpPost]
        public ActionResult EditStudent(FormCollection collection)
        {
            string strID = collection["idstudent"].Trim();
            int ID = Int32.Parse(strID);// ID của đối tg đã click
            var student = (from data in cmrs.Students where data.Student_ID == ID select data).FirstOrDefault();
            string srtName = collection["Name"].Trim();
            string srtDOB = collection["Dob"].Trim();
            string srtPhoneno = collection["Phoneno"].Trim();
            string srtAddress = collection["Address"].Trim();
            string srtMail = collection["Email"].Trim();
            string srtStatus = collection["Status"].Trim();

            int status = Int32.Parse(srtStatus.Trim());
            //Dal.Student student = new Student();
            student.StudentName = srtName;
            student.Status = status;
            student.Phone_No = srtPhoneno;
            student.Email = srtMail;
            student.Address = srtAddress;
            student.Create_Time = DateTime.Now;
            DateTime Date = Convert.ToDateTime(srtDOB.Trim());
            student.DOB = Date;
            List<Students> lst = new List<Students>();
            lst = GetAllStudent();
            Students stu = new Students();
            foreach (var item in lst)
            {
                if (item.Student_ID.Equals(ID))
                {
                    stu = item;
                }
            }
            lst.Remove(stu);
            int count = 0;
            foreach (var item in lst)
            {
                if (item.Phone_No.Equals(srtPhoneno))
                {
                    count++;
                }
            }
            if (count == 0)
            {
                try
                {
                    cmrs.SaveChanges();
                    TempData["MessageSuccess"] = "Done !";
                    return RedirectToAction("Index", "Student");
                }
                catch (Exception ex)
                {
                    TempData["MessageError"] = "Error!";
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                TempData["MessageError"] = "Phone number is exist!";
                return RedirectToAction("Index", "Student");
            }

        }
        [HttpPost]
        public ActionResult SearchStudent(FormCollection collection, int pagenum)
        {
            string strsearch = collection["search_key_news"].Trim();
            // List<Students> liststudent = PadingSearch(str)
            ViewBag.countSearch = GetAllSearch(strsearch).Count;
            if (ViewBag.countSearch == 0)
            {
                TempData["MessageError"] = "No found!";
                return RedirectToAction("Index", "Student");
            }
            return View("Search", PadingSearch(PageSize, pagenum, strsearch));
        }

        public List<Students> GetAllSearch(string search)
        {
            List<Students> lst = new List<Students>();
            var liststudent = from data in cmrs.Students
                              where data.StudentName.Contains(search) || data.Phone_No.Contains(search) || data.Email.Contains(search) || data.Address.Contains(search) || data.DOB.Value.ToString("dd/MM/yyyy").Contains(search)
                              select new
                              {

                                  data.Student_ID,
                                  data.StudentName,
                                  data.Status,
                                  data.Phone_No,
                                  data.Email,
                                  data.DOB,
                                  data.Address,
                              };
            foreach (var item in liststudent)
            {
                Students stu = new Students();
                stu.Student_ID = item.Student_ID;
                stu.StudentName = item.StudentName;
                stu.Status = item.Status;
                stu.Phone_No = item.Phone_No;
                stu.Email = item.Email;
                stu.DOB = item.DOB.Value.ToString("dd/MM/yyyy");
                // stu.DOB = item.DOB;
                stu.Address = item.Address;
                lst.Add(stu);
            }
            return lst;
        }
        public List<Students> PadingSearch(int pagesize, int pagenum, string search)
        {
            var lst = GetAllSearch(search); // pagesize =5        
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            //gỉa sử có 6 bản ghi
            // pagesize =5  nếu mà pagenum=1 trang thứ 1 thì list=lst.skip(5*) take(5))
            //
            return list;
        }
    }
}
