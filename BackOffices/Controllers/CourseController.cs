﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Models;
using BackOffices.Dal;

namespace BackOffices.Controllers
{
    public class CourseController : Controller
    {
        
        Dal.CMRSecurityEntities cmrs = new CMRSecurityEntities();
        int PageSize = 5;
        //
        // GET: /Course/

        public ActionResult Index()
        {
            return RedirectToAction("ManageCourseList", "Course", new { PageNum = 1 });
        }
        public ActionResult ManageCourseList(int pagenum)
        {

            //pagenum = 1;
            List<Course> lst = new List<Course>();

            lst = getAllCo();
            ViewBag.countnewsall = countAll();
            //  ViewBag.count = lst.Count().ToString().Trim();



            return View("Index", pading(PageSize, pagenum));
        }
        public List<Course> pading(int pagesize, int pagenum)
        {

            // status = Int32.Parse(Request.Params["variable"]);

            var lst = getAllCo(); ;
            // ViewBag.count = lst.Count().ToString().Trim();
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            return list;
        }
        public int countAll()
        {
            var query = from data in cmrs.Courses
                        select data;
            return query.Count();
        }
       
        public List<Course> getAllCo()
        {
            List<Course> lst = new List<Course>();
            var query = from data in cmrs.Courses 
                        join dat in cmrs.Faculties on data.Fa_ID equals dat.ID orderby data.Create_Time descending
                        select new
                        {
                            data.ID,
                            data.CourseName,
                            data.CourseStatus,
                            data.Fa_ID,
                            dat.FaName,
                            data.CourseCode
                        };
            foreach (var item in query)
            {
                Course fa = new Course();
                fa.ID = item.ID;
                fa.CourseName = item.CourseName;
                if (item.CourseStatus.Trim().Equals("1"))
                {
                    fa.CourseStatus = "Ready";
                }
                else {
                    fa.CourseStatus = "Block";
                }
                fa.Fa_ID = item.Fa_ID;
                fa.Faculty = item.FaName;
                fa.CourseCode = item.CourseCode;
                lst.Add(fa);
            }
            return lst;
        }

      
        public ActionResult Add()
        {
            //List<SelectListItem> list = new List<SelectListItem>();
            //list.Add(new SelectListItem { Text = "-Please select-", Value = "Selects items" });
            var cat = (from c in cmrs.Faculties select c).ToArray();
            //for (int i = 0; i < cat.Length; i++)
            //{
            //    list.Add(new SelectListItem
            //    {
            //        Text = cat[i].FaName,
            //        Value = cat[i].ID.ToString(),
            //        Selected = (cat[i].ID == 1)
            //    });
            //}
            //ViewData["List"] = list;
            ViewData["a"] = cat;
            return View("Add");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNewCo(FormCollection collection)
        {
            List<Course> lst = new List<Course>();
            lst = getAllCo();

            string strname = collection["Name"];
            string strcode = collection["Code"];
            string stt = collection["stt"];
            string intfa= collection["combobox"];
            int id = Int32.Parse(intfa);
            DateTime time = DateTime.Now;
            var fa = new Cours();
            fa.CourseName = strname;
            fa.CourseStatus = stt;
            fa.Fa_ID = id;
            fa.CourseCode = strcode;
            fa.Create_Time = time;
            var count = 0;
            foreach (var data in lst)
            {
                if ((data.CourseName.ToLower()).Equals(strname.ToLower()))
                {
                    count = 1;
                }
            }
            try
            {
                if (count == 0)
                {
                    cmrs.Courses.Add(fa);
                    cmrs.SaveChanges();
                    TempData["MessageSuccess"] = "Success !";
                    return RedirectToAction("Index", "Course");
                }
                else
                {
                    TempData["MessageError"] = "Error name exist!";
                    return RedirectToAction("Index", "Course");
                }
            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
        }


        public ActionResult DeleteCo(int id)
        {
            //int variable =Int32.Parse( Request.Params["variable"]);
            var co = (from data in cmrs.Courses
                      where data.ID == id
                      select data).FirstOrDefault();
            try
            {


                cmrs.Courses.Remove(co);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "Error!";
                throw new Exception(ex.Message);
            }
            return RedirectToAction("Index", "Course");
        }

        public ActionResult GotoUpdate(int id)
        {

            var query = (from data in cmrs.Courses
                         where data.ID == id
                         select data).FirstOrDefault();
            var cat = (from c in cmrs.Faculties  select c).ToArray();

            ViewData["a"] = cat;


            return View("Edit", query);

        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateCo(FormCollection collection)
        {
            int page = Int32.Parse(collection["page"].Trim());
            int idco = Int32.Parse(collection["idco"].Trim());
            var co = (from data in cmrs.Courses
                      where data.ID == idco
                      select data).FirstOrDefault();

            List<Course> lst = new List<Course>();
            lst = getAllCo();
            var re = new Course();
            foreach (var data in lst)
            {
                if (data.ID == co.ID)
                {
                    re = data;
                }
            }
            lst.Remove(re);
            string strname = collection["Name"];
            string strcode = collection["Code"];
            string stt = collection["stt"];
            string intfa = collection["combobox"];
            int id = Int32.Parse(intfa);
            DateTime time = DateTime.Now;
            
            co.CourseName = strname;
            co.CourseStatus = stt;
            co.Fa_ID = id;
            co.CourseCode = strcode;
            co.Create_Time = time;
            var count = 0;
            foreach (var data in lst)
            {
                if ((data.CourseName.ToLower()).Equals(strname.ToLower()))
                {
                    count = 1;
                }
            }
            try
            {
                if (count == 0)
                {
                   
                    cmrs.SaveChanges();
                    TempData["MessageSuccess"] = "Success !";
                    return RedirectToAction("Index", "Course", new { PageNum = page });
                }
                else
                {
                    TempData["MessageError"] = "Error name exist!";
                    return RedirectToAction("Index", "Course", new { PageNum = page });
                }
            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
        }

        public ActionResult SearchCourse(int pagenum, FormCollection collection)
        {
            string strSearch = collection["search_key_news"].Trim();
            ViewBag.countSearch = getAllSearch(strSearch).Count;
            ViewBag.search_key_news = strSearch;
            return View("Search", padingSearch(PageSize, pagenum, strSearch));
        }
        public List<Course> getAllSearch(string search)
        {
            List<Course> lst = new List<Course>();
            var query = from data in cmrs.Courses join
                        dat in cmrs.Faculties on data.Fa_ID equals dat.ID
                        where (data.CourseName.ToLower().Contains(search.Trim().ToLower())||(data.CourseCode.ToLower().Contains(search.Trim().ToLower())))
                        select new
                        {
                            data.ID,
                            data.CourseName,
                            data.CourseStatus,
                            data.Fa_ID,
                            dat.FaName,
                            data.CourseCode
                        };
            foreach (var item in query)
            {
                Course fa = new Course();
                fa.ID = item.ID;
                fa.CourseName = item.CourseName;
                if (item.CourseStatus.Trim().Equals("1"))
                {
                    fa.CourseStatus = "Ready";
                }
                else
                {
                    fa.CourseStatus = "Block";
                }
                fa.Fa_ID = item.Fa_ID;
                fa.Faculty = item.FaName;
                fa.CourseCode = item.CourseCode;
                lst.Add(fa);
            }

            return lst;
        }

        public List<Course> padingSearch(int pagesize, int pagenum, string search)
        {
            ViewBag.search_key_news = search;
            var lst = getAllSearch(search.Trim());
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            return list;
        }
    }
}
