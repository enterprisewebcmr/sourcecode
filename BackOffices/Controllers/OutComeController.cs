﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Dal;
using BackOffices.Models;
using System.Net.Mail;

namespace BackOffices.Controllers
{
    public class OutComeController : Controller
    {
        //
        // GET: /OutCome/
        Dal.CMRSecurityEntities cmrs = new Dal.CMRSecurityEntities();
        private object ssCPY_ID = System.Web.HttpContext.Current.Session["CPY_ID"];
        public ActionResult Index()
        {   

            return RedirectToAction("ManageOC", "OutCome");
        }
        public ActionResult ManageOC(int? id, int? PageNum, int? Student_ID)
        {
            ViewBag.StudentID = Student_ID;
            ViewBag.PageNum = PageNum;
            string year = DateTime.Now.Year.ToString();
            var countstudent = from data in cmrs.CPY_Student 
                               join dat in cmrs.CoursePerYears on data.CoursePerYear_ID equals dat.ID
                               join da in cmrs.Students on data.Student_ID equals da.Student_ID
                               where data.CoursePerYear_ID == id&& dat.Year.Equals(year) select data;
            int count = countstudent.ToList().Count;
            ViewBag.CountStudent = count;
            SetValue("CPY_ID", id); 
            List<OutComes> lst = new List<OutComes>(); 

            lst = getListOCById(id);







            var query = (from data in cmrs.Courses
                         join
                             dat in cmrs.CoursePerYears on data.ID equals dat.CourseID
                         join
                             da in cmrs.Accounts on dat.CM_ID equals da.ID
                         join
                             d in cmrs.Accounts on dat.CL_ID equals d.ID
                         where dat.ID==id
                         select new
                         {
                             ID = dat.ID,
                             CL_ID = dat.CL_ID,
                             CM_ID = dat.CM_ID,
                             Create_Time = dat.Create_Time,
                             CourseID = dat.CourseID,
                             CPY_Code = dat.CPY_Code,
                             Status = dat.Status,
                             Year = dat.Year,
                             CourseName = data.CourseName,
                             CM_FullName = da.FullName,
                             CL_FullName = d.FullName
                         }).FirstOrDefault();

             CPY cpy = new CPY();
                cpy.ID = query.ID;
                cpy.Status = query.Status;
                cpy.Year = query.Year;
                cpy.CM_Name = query.CM_FullName;
                cpy.CourseName = query.CourseName;
                cpy.CPY_Code = query.CPY_Code;
                cpy.CL_Name = query.CL_FullName;
             
           
            ViewData["cpy"] = cpy;
            ViewData["count"] = CountPerCent(id);



            List<string> _distributionFieldGrade = new List<string>();
            _distributionFieldGrade.Add("       ");
            _distributionFieldGrade.Add("0-9");
            _distributionFieldGrade.Add("10-19");
            _distributionFieldGrade.Add("20-29");
            _distributionFieldGrade.Add("30-39");
            _distributionFieldGrade.Add("40-49");
            _distributionFieldGrade.Add("50-59");
            _distributionFieldGrade.Add("60-69");
            _distributionFieldGrade.Add("70-79");
            _distributionFieldGrade.Add("80-89");
            _distributionFieldGrade.Add("90+");

            ViewBag.DistributionFieldGrade = _distributionFieldGrade;

            return View("IndexOutCome",lst);
        }


        [HttpPost]
        public ActionResult WriteComment(string txtWriteComment)
        {

            string post = txtWriteComment;


            MailMessage mail = new System.Net.Mail.MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress("cmrsg3@gmail.com");

            mail.To.Add("cmrsg3@gmail.com");

            mail.Subject = "Course #" + Request.UrlReferrer.ToString();

            mail.Body = post + "\r\n" + Request.UrlReferrer.ToString();



            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("cmrsg3@gmail.com", "cmrs2016");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);



            TempData["MessageSuccess"] = "Send Mail successfull!";







            return Redirect(Request.UrlReferrer.ToString());
        }



        public Dictionary<string, string> DistrubutionOutcomeGrade(int cpyid, string outcomeName)
        {
            List<int> stuidList = new List<int>();
            List<float> overralList = new List<float>();


            var query = (from data in cmrs.CoursePerYears
                         join oc in cmrs.OutComes on data.CourseID equals oc.CoupID
                         join ts in cmrs.Transcripts on oc.ID equals ts.OC_ID
                         where data.CourseID == cpyid && oc.OutcomeName == outcomeName
                         select new
                         {
                             ID = ts.StudentID,
                             Score = ts.Score,
                             Percen = oc.Percentage,
                         });




            var _query = query.GroupBy(p => p.Score).Select(g => g.FirstOrDefault());
            foreach (var item in _query)
            {
                int? stuID = item.ID; int stuidFix = stuID.GetValueOrDefault();
                float score = (float)item.Score;
                overralList.Add(score);

            }
            int sc10 = 0;
            int sc20 = 0;
            int sc30 = 0;
            int sc40 = 0;
            int sc50 = 0;
            int sc60 = 0;
            int sc70 = 0;
            int sc80 = 0;
            int sc90 = 0;
            int sc100 = 0;
            int size = overralList.Count;
            for (int x = 0; x < size; x++)
            {
                float scorecur = overralList[x];
                if (x < 10f)
                {
                    sc10++;
                }

                if (x < 20f && x >= 10f)
                {
                    sc20++;
                }

                if (x < 30 && x >= 20f)
                {
                    sc30++;
                }

                if (x < 40f && x >= 30f)
                {
                    sc40++;
                }
                if (x < 50f && x >= 40f)
                {
                    sc50++;
                }
                if (x < 60f && x >= 50f)
                {
                    sc60++;
                }
                if (x < 70f && x >= 60f)
                {
                    sc70++;
                }
                if (x < 80f && x >= 70f)
                {
                    sc80++;
                }
                if (x < 90f && x >= 80f)
                {
                    sc90++;
                }
                else
                {
                    sc100++;
                }

            }


            Dictionary<string, string> _distribution = new Dictionary<string, string>();

            _distribution.Add("0-9", sc10.ToString());
            _distribution.Add("10-19", sc20.ToString());
            _distribution.Add("20-29", sc30.ToString());
            _distribution.Add("30-39", sc40.ToString());
            _distribution.Add("40-49", sc50.ToString());
            _distribution.Add("50-59", sc60.ToString());
            _distribution.Add("60-69", sc70.ToString());
            _distribution.Add("70-79", sc80.ToString());
            _distribution.Add("80-89", sc90.ToString());
            _distribution.Add("90+", sc100.ToString());


            return _distribution;

        }



        public void DistrubutionOverallGrade(int cpyid)
        {
            List<int> stuidList = new List<int>();
            List<float> overralList = new List<float>();


            var query = (from data in cmrs.CoursePerYears
                         join oc in cmrs.OutComes on data.CourseID equals oc.CoupID
                         join ts in cmrs.Transcripts on oc.ID equals ts.OC_ID
                         where data.CourseID == cpyid
                         select new
                         {
                             ID = ts.StudentID,
                             Score = ts.Score,
                             Percen = oc.Percentage,
                         });

            foreach (var item in query)
            {
                int? stuID = item.ID; int stuidFix = stuID.GetValueOrDefault();
                int? score = item.Score;
                float scorenew = Convert.ToSingle(item.Percen * score / 100);
                int index = stuidList.IndexOf(stuidFix);
                if (index >= 0)
                {
                    overralList[index] = overralList[index] + scorenew;

                }
                else
                {
                    stuidList.Add(stuidFix);
                    overralList.Add(scorenew);
                }
            }
            int sc10 = 0;
            int sc20 = 0;
            int sc30 = 0;
            int sc40 = 0;
            int sc50 = 0;
            int sc60 = 0;
            int sc70 = 0;
            int sc80 = 0;
            int sc90 = 0;
            int sc100 = 0;
            int size = overralList.Count;
            for (int x = 0; x < size; x++)
            {
                float scorecur = overralList[x];
                if (x < 10f)
                {
                    sc10++;
                }

                if (x < 20f && x >= 10f)
                {
                    sc20++;
                }

                if (x < 30 && x >= 20f)
                {
                    sc30++;
                }

                if (x < 40f && x >= 30f)
                {
                    sc40++;
                }
                if (x < 50f && x >= 40f)
                {
                    sc50++;
                }
                if (x < 60f && x >= 50f)
                {
                    sc60++;
                }
                if (x < 70f && x >= 60f)
                {
                    sc70++;
                }
                if (x < 80f && x >= 70f)
                {
                    sc80++;
                }
                if (x < 90f && x >= 80f)
                {
                    sc90++;
                }
                else
                {
                    sc100++;
                }

            }

        }



        public int CountPerCent(int? id){
            var query = from data in cmrs.OutComes where data.CoupID == id select data;
            int count = 0;
            foreach (var item in query) {
                count = count + item.Percentage;
            }
            return count;
        }

        public List<OutComes> getListOCById(int? cpyid)
        {
            List<OutComes> lst = new List<OutComes>();
            var query = (from data in cmrs.OutComes
                         
                        where data.CoupID == cpyid
                         select new
                         {
                             ID = data.ID,
                             OC_name = data.OutcomeName,
                             Percent = data.Percentage,
                             CPY_ID=data.CoupID

                         });

            foreach (var item in query)
            {
                OutComes oc = new OutComes();
                oc.ID = item.ID;
                oc.CPY_ID = item.CPY_ID;
                oc.OutCome_Name = item.OC_name;
                oc.Percent = item.Percent;


                oc.DistributionGrade = DistrubutionOutcomeGrade(item.CPY_ID, item.OC_name);






                lst.Add(oc);
            }
            return lst;
        }

        public ActionResult AddNew(int id)
        {
           
            List<OutComes> lst = new List<OutComes>();
            lst = getListOCById(Int32.Parse(ssCPY_ID.ToString()));
            int CPY_ID = Int32.Parse(ssCPY_ID.ToString());
            int count = 0;
            foreach (var data in lst) {
                count = count + data.Percent;
            }
            if (count >= 100) {
                TempData["MessageError"] = "percent do not larger than 100%!";
                return RedirectToAction("ManageOC", "OutCome", new { id = CPY_ID });
            }
            int countper = 0;
            foreach (var data in lst)
            {
                countper = countper + data.Percent;
            }
            ViewData["countadd"] = 100 - count;   
            return View("AddOutCome", new { CPY_ID=id});
        }

        public static void SetValue(string name, object value)
        {
            System.Web.HttpContext.Current.Session.Add(name, value);
        }

        // lấy session
        public static object GetValue(string name)
        {
            return System.Web.HttpContext.Current.Session[name] == null ? null : System.Web.HttpContext.Current.Session[name];
        }

        // xóa session
        public static void Remove(string name)
        {
            System.Web.HttpContext.Current.Session.Remove(name);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNewOC(FormCollection collection)
        {
            int CPY_ID = Int32.Parse(ssCPY_ID.ToString());
            string strname = collection["Name"];
            int percent = Int32.Parse(collection["Percent"]);
            List<OutComes> lst = new List<OutComes>();
            lst = getListOCById(CPY_ID);
            int count = 0;
            foreach (var a in lst) {
                count = count + a.Percent;
            }
            if (count+percent > 100) {
                TempData["MessageError"] = "percent do not larger than 100%!";
                return RedirectToAction("ManageOC", "OutCome", new { id = CPY_ID });
            }
            else if (count+percent <= 100) { 
               
                var oc = new OutCome();
                oc.CoupID = CPY_ID;
                oc.OutcomeName = strname.Trim();
                oc.Percentage = percent;
                var flag = 0;
                foreach (var data in lst)
                {
                    if ((data.OutCome_Name.ToLower()).Equals(strname.ToLower()))
                    {
                        flag = 1;
                    }

                }
                try
                {
                    if (flag == 0)
                    {
                        cmrs.OutComes.Add(oc);
                        cmrs.SaveChanges();
                       // Remove("CPY_ID");
                        TempData["MessageSuccess"] = "Success !";
                        return RedirectToAction("ManageOC", "OutCome", new { id = CPY_ID });
                    }
                    else
                    {
                        TempData["MessageError"] = "Error name exist!";
                        return RedirectToAction("ManageOC", "OutCome", new { id = CPY_ID });
                    }
                }
                catch (Exception e)
                {
                    TempData["MessageError"] = "Error !";
                    throw new Exception(e.Message);
                }

            }
            
            
           
            return View();
        }

        public ActionResult Delete(int id)
        {
            int CPY_ID = Int32.Parse(ssCPY_ID.ToString());

            var oc = (from data in cmrs.OutComes
                      where data.ID == id
                      select data).FirstOrDefault();
            try
            {


                cmrs.OutComes.Remove(oc);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "Error!";
                throw new Exception(ex.Message);
            }
            return RedirectToAction("ManageOC", "OutCome", new { id = CPY_ID });
        }

        public ActionResult GotoUpdate(int id)
        {

            var query = (from data in cmrs.OutComes
                         where data.ID == id
                         select data).FirstOrDefault();           
            List<OutComes> lst = new List<OutComes>();
            lst = getListOCById(Int32.Parse(ssCPY_ID.ToString()));
            var re = new OutComes();
            foreach (var data in lst)
            {
                if (data.ID == query.ID)
                {
                    re = data;
                }
            }
            lst.Remove(re);
            int count = 0;
            foreach (var data in lst) {
                count = count + data.Percent;
            }
            ViewData["count"] =100-count;   
            return View("EditOutCome", query);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditOC(FormCollection collection)
        {
            int CPY_ID = Int32.Parse(ssCPY_ID.ToString());
            int id = Int32.Parse(collection["idfa"].Trim());
            var oc = (from data in cmrs.OutComes

                      where data.ID == id
                      select data).FirstOrDefault();

            List<OutComes> lst = new List<OutComes>();
            lst = getListOCById(Int32.Parse(ssCPY_ID.ToString()));
            var re = new OutComes();
            foreach (var data in lst)
            {
                if (data.ID == oc.ID)
                {
                    re = data;
                }
            }
            lst.Remove(re);
            string strname = collection["Name"];
            int percent = Int32.Parse(collection["Percent"]);
            oc.OutcomeName = strname;
            oc.Percentage = percent;
            
            var count = 0;
            foreach (var data in lst)
            {
                if ((data.OutCome_Name.ToLower()).Equals(strname.ToLower()))
                {
                    count = 1;
                }

            }
            try
            {
                if (count == 0)
                {

                    cmrs.SaveChanges();
                    TempData["MessageSuccess"] = "Success !";
                    return RedirectToAction("ManageOC", "OutCome", new { id = CPY_ID });
                }
                else
                {
                    TempData["MessageError"] = "Error name exist!";
                    return RedirectToAction("ManageOC", "OutCome", new { id = CPY_ID });
                }
            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
        }
    }
}
