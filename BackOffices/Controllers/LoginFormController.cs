﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using Security.Foundation.Data.Implements;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.WrapModels;

using Security.Foundation.Utility;

using System.Web.Script.Serialization;

namespace BackOffices.Controllers
{
    public class LoginFormController : Controller
    {
        private IAccountImpl _IAccountImpl;
        private IAccountRoleImpl _IAccountRoleImpl;
        public LoginFormController(IAccountImpl __IAccountImpl, 
            IAccountRoleImpl __IAccountRoleImpl)
        {
            _IAccountImpl = __IAccountImpl;
            _IAccountRoleImpl = __IAccountRoleImpl;
        }

        public ActionResult Index()
        {

            //_IAccountImpl.Add(new Account { Email="hakhanhlong@gmail.com", Locked=false, UserName="admin", SupperAdmin=true, FullName="Ha Khanh Long", Password=AccountCrypto.Encrypt("111111", "admin"), CreatedDate=DateTime.Now});

            if (Request.QueryString["ReturnUrl"] != null)
            {
                ViewBag.ReturnURL = Request.QueryString["ReturnUrl"];
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(Account model, string returnUrl, int chkRememberPass = 0)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(model.UserName))
                    {
                        model.UserName = model.UserName.ToLower();
                        var _account = _IAccountImpl.GetAccountByUserName(model.UserName);
                        if (_account != null)
                        {
                            if (_account.Locked)
                            {
                                TempData["MessageError"] = "\"" + _account.UserName + "\" đã bị khóa.";
                            }
                            else
                            {
                                if (_account.Password == AccountCrypto.Encrypt(model.Password, model.UserName))
                                {
                                    // role array
                                    var _arrRoleID = _IAccountRoleImpl.GetByAccountID(_account.ID).Select(a => a.RoleID);
                                    var _userData = new WrapUserData { RoleIDs = _arrRoleID.ToArray(), SupperAdmin = _account.SupperAdmin, Username = _account.UserName, AccountID = _account.ID };
                                    string _jsonUserData = new JavaScriptSerializer().Serialize(_userData);
                                    //######################################################

                                    var authenTicket = new FormsAuthenticationTicket(1, string.IsNullOrWhiteSpace(_account.FullName) ? _account.UserName : _account.FullName, DateTime.Now, DateTime.Now.AddDays(30), chkRememberPass == 1 ? true : false, _jsonUserData);
                                    string cookieTicket = FormsAuthentication.Encrypt(authenTicket);
                                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieTicket)
                                    {
                                        Expires = authenTicket.Expiration,
                                        Path = FormsAuthentication.FormsCookiePath
                                    };

                                    Response.Cookies.Add(cookie);



                                    if (!string.IsNullOrEmpty(returnUrl))
                                    {
                                        return Redirect(returnUrl);
                                    }
                                    else
                                    {
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                                else {
                                    
                                    TempData["MessageError"] = "Mật khẩu không đúng.";
                                }
                            }
                        }
                        else
                        {
                            TempData["MessageError"] = "Tên đăng nhập không tồn tại.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["MessageError"] = ex.InnerException;
                }
            }

            return View(model);
        }



    }
}
