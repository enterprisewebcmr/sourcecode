﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Models;
using BackOffices.Dal;

namespace BackOffices.Controllers
{
    public class TranscriptController : Controller
    {
        //
        // GET: /Transcript/
        Dal.CMRSecurityEntities cmrs = new Dal.CMRSecurityEntities();
        public ActionResult Index()
        {
            return RedirectToAction("ManageTranscipt", "Transcript");
        }
        public ActionResult ManageTranscipt(int id){
            var query = (from data in cmrs.OutComes where data.ID == id select data).FirstOrDefault();
            OutComes oc = new OutComes();
            oc.OutCome_Name = query.OutcomeName;
            oc.Percent = query.Percentage;
            oc.ID = query.ID;
            ViewData["oc"] = oc;
           // get all student learn in course
            var list = from data in cmrs.Students
                       join dat in cmrs.CPY_Student on data.Student_ID equals dat.Student_ID
                       join da in cmrs.CoursePerYears on dat.CoursePerYear_ID equals da.ID
                       join d in cmrs.OutComes on da.ID equals d.CoupID
                       
                       where d.ID == id
                       select new {
                           data.Student_ID,
                           data.StudentName,
                           d.ID
                       };

            List<Transcripts> lists = new List<Transcripts>();
            foreach (var item in list)
            {
                Transcripts st = new Transcripts();
               st.Student_ID=item.Student_ID;
                st.Student_Name = item.StudentName;
                st.Mark = null;
                lists.Add(st);
            }
            //get student learn in course and have mark

            var studentmark = from data in cmrs.Transcripts
                           join dat in cmrs.OutComes on data.OC_ID equals dat.ID
                           join da in cmrs.Students on data.StudentID equals da.Student_ID
                           where dat.ID == id
                           select new
                           {    data.StudentID,
                               data.Transcript_ID,
                               da.StudentName,
                               data.Score
                           };

            List<Transcripts> listjoins = new List<Transcripts>();
            foreach (var item in studentmark)
            {
                Transcripts tr = new Transcripts();
                tr.Student_ID=item.StudentID;
                tr.Tran_ID = item.Transcript_ID;
                tr.Student_Name = item.StudentName;
                tr.Mark = item.Score;
                listjoins.Add(tr);
            }
            lists.RemoveAll(x => listjoins.Any(y => y.Student_ID.Equals(x.Student_ID)));
            //int countlj = listjoins.Count;
            ViewData["lists"] = listjoins;
                return View("Index", lists);
            
        }
        public ActionResult Add(int Student_ID, int OC_ID)
        {
            var stu = (from data in cmrs.Students where data.Student_ID== Student_ID select data).FirstOrDefault();
            ViewBag.Student_ID = Student_ID;
            ViewBag.OC_ID = OC_ID;
            //Students st = new Students();
            //st.StudentName = stu.StudentName;
            
            return View("Add",stu);
        }
        [HttpPost]
        public ActionResult Add(FormCollection collection)
        {
            string studen_id = collection["idstudent"].Trim();
            string oc_id = collection["idoc"].Trim();
            string mark = collection["Mark"];
            Transcript ts = new Transcript();
            ts.StudentID = Int32.Parse(studen_id);
            ts.OC_ID = Int32.Parse(oc_id);
            ts.Score = Int32.Parse(mark);
            try
            {
                cmrs.Transcripts.Add(ts);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
                return RedirectToAction("ManageTranscipt", "Transcript", new { id = Int32.Parse(oc_id) });
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "error!";
                throw new Exception(ex.Message);
            }

           
        }
        public ActionResult Update(int Tran_ID, int OC_ID)
        {
            var stu = (from data in cmrs.Students 
                       join dat in cmrs.Transcripts on data.Student_ID equals dat.StudentID
                       where  dat.Transcript_ID==Tran_ID
                       select new
                       {
                         data.StudentName,
                         dat.Score
                        }).FirstOrDefault();
           
         Transcripts tr = new Transcripts();
            tr.Tran_ID = Tran_ID;
            tr.Student_Name = stu.StudentName;
            tr.Mark = stu.Score;
            tr.OC_ID = OC_ID;
            return View("Edit", tr);
         
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection) {
            string tranid = collection["idtran"];
            int Tran_ID=Int32.Parse(tranid);
            string mark = collection["Mark"];
            string oc_id = collection["idoc"];
            var tran = (from data in cmrs.Transcripts where data.Transcript_ID == Tran_ID select data).FirstOrDefault();
            tran.Score = Int32.Parse(mark);
            try
            {
              
                    cmrs.SaveChanges();
                    TempData["MessageSuccess"] = "Success !";
                    return RedirectToAction("ManageTranscipt", "Transcript", new { id = Int32.Parse(oc_id) });
               
            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
           
        }
    }
}
