﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Dal;
using BackOffices.Models;

namespace BackOffices.Controllers
{
    public class CPY_StudentController : Controller
    {
        //
        // GET: /CPY_Student/
        Dal.CMRSecurityEntities cmrs = new Dal.CMRSecurityEntities();
        public ActionResult Index()
        {
            return RedirectToAction("Manage_CPY_Student", "CPY_Student");
        }
        public ActionResult Manage_CPY_Student(int id,int PageNum) {
            ViewBag.Num = PageNum;
            List<BackOffices.Models.CPY_Students> lst = new List<CPY_Students>();
            lst = GetListCPY_StudentsById(id);
            var query = (from data in cmrs.Students
                        
                         where data.Student_ID == id
                         select new
                         {
                             data.StudentName,
                             data.Student_ID,
                             data.DOB,
                             data.Address,
                             data.Email,
                             data.Phone_No,
                             

                         }).FirstOrDefault();
            Students student = new Students();
            student.Student_ID = query.Student_ID;
            student.StudentName = query.StudentName;
            student.Phone_No = query.Phone_No;
            student.Email = query.Email;
            student.DOB = query.DOB.Value.ToString("dd/MM/yyyy");
            student.Address = query.Address;
            ViewData["student"] = student;
            ViewData["CPY_Student_ByID"] = lst;
            ViewData["AllCPY_Student"] = GetAllCPY_Students(id);
            return View("Index", lst);
            
        }
        public List<CPY_Students> GetListCPY_StudentsById(int id) {
            //chekc cos cpy-stu-id
            List<CPY_Students> lst = new List<CPY_Students>();
            var query = from  data in cmrs.CoursePerYears
                        join dat in cmrs.Courses on data.CourseID equals dat.ID
                        join da in cmrs.CPY_Student on data.ID equals da.CoursePerYear_ID
                        join d in cmrs.Students on da.Student_ID equals d.Student_ID
                        where da.Student_ID == id
                        select new
                        {
                            CPY_Code = data.CPY_Code,
                            CPY_year = data.Year,
                            Course_Name = dat.CourseName,
                            CPY_ID = data.ID,
                            Student_ID = id,
                            CPY_Student_ID = da.ID
                        };
            foreach (var item in query)
            {
                CPY_Students cpys = new CPY_Students();
                cpys.Student_ID = item.Student_ID;
                cpys.Year = item.CPY_year;
                cpys.CPY_Stdent_ID = item.CPY_Student_ID;
                cpys.CPY_Code = item.CPY_Code;
               cpys.CPY_Name = item.Course_Name;
               cpys.CPY_ID = item.CPY_ID;
                lst.Add(cpys);
            }

          
          
            return lst;
        }
        public List<CPY_Students> GetAllCPY_Students(int id)
        {
           

            string stryear = DateTime.Now.Year.ToString();
           // DateTime currentyear = Convert.ToDateTime(stryear);
            List<CPY_Students> lst = new List<CPY_Students>();
            var query = from data in cmrs.CoursePerYears
                        join dat in cmrs.Courses on data.CourseID equals dat.ID
                        //join da in cmrs.CPY_Student on data.ID equals da.CoursePerYear_ID
                        where data.Year.Equals(stryear)
                        select new
                        {
                            data.ID,
                            data.CPY_Code,
                            dat.CourseName,
                            data.Year,
                          // CPY_Student_ID= da.ID
                        };
            foreach (var item in query)
            {
                CPY_Students cpys = new CPY_Students();
                //cpys.Student_ID = item.Student_ID;
                cpys.Year = item.Year;
                cpys.CPY_ID = item.ID;
                cpys.CPY_Code = item.CPY_Code;
                cpys.CPY_Name = item.CourseName;
                //cpys.CPY_Stdent_ID = item.CPY_Student_ID;
                lst.Add(cpys);
            }
            //List<CPY_Students> lst = new List<CPY_Students>();
            //var query = from
            //              data in cmrs.CoursePerYears
            //            join dat in cmrs.Courses on data.CourseID equals dat.ID
            //            join
            //            da in cmrs.CPY_Student
            //            on data.ID equals da.CoursePerYear_ID
            //            join d in cmrs.Students on da.Student_ID equals d.Student_ID
            //            where data.Year.Equals(stryear)
            //            select new
            //            {
            //                CPY_Code = data.CPY_Code,
            //                CPY_year = data.Year,
            //                Course_Name = dat.CourseName,
            //                CPY_ID = data.ID,
            //                Student_ID = id,
            //                CPY_Student_ID = da.ID
            //            };
            //foreach (var item in query)
            //{
            //    CPY_Students cpys = new CPY_Students();
            //    cpys.Student_ID = item.Student_ID;
            //    cpys.Year = item.CPY_year;
            //    cpys.CPY_Stdent_ID = item.CPY_Student_ID;
            //    cpys.CPY_Code = item.CPY_Code;
            //    cpys.CPY_Name = item.Course_Name;
            //    lst.Add(cpys);
            //}

            return lst;
        }
        public ActionResult AddCPY_Student(int id)
        {
            List<CPY_Students> list = new List<CPY_Students>();
            list = ListOfCourse(id);
            ViewBag.Student_ID = id;
            ViewData["a"] = list;
            return View("AddCPY_Student");
        }
        public List<CPY_Students> ListOfCourse(int id) {
            List<CPY_Students> list = new List<CPY_Students>();
            List<CPY_Students> lst = new List<CPY_Students>();
            list = GetAllCPY_Students(id);
            lst = GetListCPY_StudentsById(id);
            // list.RemoveAll(i => lst.Contains(i));
            //var result = list.Except(lst);
            list.RemoveAll(x => lst.Any(y => y.CPY_Code.Equals(x.CPY_Code)));
            return list;
        }
        [HttpPost]
        public ActionResult AddCPY_Student(FormCollection collection) {
            string strCPY_ID = collection["CPY"];
            string strStudent_ID = collection["studentid"];
            Dal.CPY_Student cpys = new CPY_Student();
            cpys.CoursePerYear_ID = Int32.Parse(strCPY_ID);
            
            cpys.Student_ID = Int32.Parse(strStudent_ID);
            if (strStudent_ID.Equals("0")) {
               
                return View("AddCPY_Student");
            }
            try
            {
                cmrs.CPY_Student.Add(cpys);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
                return RedirectToAction("Manage_CPY_Student", "CPY_Student", new { id = cpys.Student_ID });
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "Error!";
                throw new Exception(ex.Message);
            }
        }
        public ActionResult RemoveCourse(int Student_ID, int CPY_Student_ID)
        {
            var cpys = (from data in cmrs.CPY_Student where data.ID == CPY_Student_ID select data).FirstOrDefault();
            try
            {
                cmrs.CPY_Student.Remove(cpys);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
                return RedirectToAction("Manage_CPY_Student", "CPY_Student", new { id = Student_ID });
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "Error!";
                throw new Exception(ex.Message);
            }
            //return RedirectToAction("Manage_CPY_Student", "CPY_Student", new { id = Student_ID });
        }
    }
}
