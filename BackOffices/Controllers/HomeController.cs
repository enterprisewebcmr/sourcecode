﻿using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.WrapModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace BackOffices.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        private IMenuPageImpl _IMenuPageImpl;
        public HomeController(IMenuPageImpl __IMenuPageImpl)
        {
            _IMenuPageImpl = __IMenuPageImpl;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NavigatorMenu()
        {

            try
            {
                var ticketData = ((FormsIdentity)HttpContext.User.Identity).Ticket.UserData;
                WrapUserData _WrapUserData = new JavaScriptSerializer().Deserialize<WrapUserData>(ticketData);
                // is supper admin fill all menu
                if (_WrapUserData.SupperAdmin)
                {
                    var results = _IMenuPageImpl.GetAllMenuPage().ToList();
                    if (results != null)
                    {
                        List<WrapMenuPage> _WrapMenuPages = new List<WrapMenuPage>();
                        foreach (IGrouping<string, MenuPage> item in results)
                        {
                            _WrapMenuPages.Add(new WrapMenuPage
                            {
                                MenuName = item.Key,
                                MenuPages = item.Distinct().ToList()
                            });
                        }

                        return View(_WrapMenuPages);

                    }
                }
                else
                {
                    // check by roleids
                    var results = _IMenuPageImpl.GetMenuPageByRoleID(_WrapUserData.RoleIDs).ToList();
                    if (results != null)
                    {
                        List<WrapMenuPage> _WrapMenuPages = new List<WrapMenuPage>();
                        foreach (IGrouping<string, MenuPage> item in results)
                        {
                            _WrapMenuPages.Add(new WrapMenuPage
                            {
                                MenuName = item.Key,
                                MenuPages = item.Distinct().ToList()
                            });
                        }

                        return View(_WrapMenuPages);

                    }
                }


            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return View(new List<WrapMenuPage>());
        }



    }
}
