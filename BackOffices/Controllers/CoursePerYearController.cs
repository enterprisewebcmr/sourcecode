﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Dal;
using BackOffices.Models;
namespace BackOffices.Controllers
{
    public class CoursePerYearController : Controller
    {
        //
        // GET: /CoursePerYear/
        Dal.CMRSecurityEntities cmrs = new Dal.CMRSecurityEntities();
        private int PageSize = 5;
        public ActionResult Index()
        {
            return RedirectToAction("ManageCPYList", "CoursePerYear", new { PageNum = 1 });
        }
        public ActionResult ManageCPYList(int pagenum)
        {

            //pagenum = 1;
            List<BackOffices.Models.CPY> lst = new List<Models.CPY>();

            lst = getAllCPY();
            ViewBag.countnewsall = countAll();
            //  ViewBag.count = lst.Count().ToString().Trim();



            return View("Index", pading(PageSize, pagenum));
        }
        public List<CPY> pading(int pagesize, int pagenum)
        {

            // status = Int32.Parse(Request.Params["variable"]);

            var lst = getAllCPY(); ;
            // ViewBag.count = lst.Count().ToString().Trim();
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            return list;
        }
        public int countAll()
        {
            var query = from data in cmrs.CoursePerYears

                        select data;
            return query.Count();
        }
        public List<CPY> getAllCPY()
        {
            List<CPY> lst = new List<CPY>();
            var query = (from data in cmrs.Courses
                         join
                             dat in cmrs.CoursePerYears on data.ID equals dat.CourseID
                         join
                             da in cmrs.Accounts on dat.CM_ID equals da.ID
                         join
                             d in cmrs.Accounts on dat.CL_ID equals d.ID
                         //orderby int.Parse(dat.Year) //,dat.Create_Time descending
                         select new
                         {
                             ID = dat.ID,
                             CL_ID = dat.CL_ID,
                             CM_ID = dat.CM_ID,
                             Create_Time = dat.Create_Time,
                             CourseID = dat.CourseID,
                             CPY_Code = dat.CPY_Code,
                             Status = dat.Status,
                             Year = dat.Year,
                             CourseName = data.CourseName,
                             CM_FullName = da.FullName,
                             CL_FullName = d.FullName

                         }).Distinct().OrderBy(x => x.Year);
            foreach (var item in query)
            {
                CPY cpy = new CPY();
                cpy.ID = item.ID;
                cpy.Status = item.Status;
                cpy.Year = item.Year;
                cpy.CM_Name = item.CM_FullName;
                cpy.CourseName = item.CourseName;
                cpy.CPY_Code = item.CPY_Code;
                cpy.CL_Name = item.CL_FullName;
                lst.Add(cpy);
            }
            return lst;
        }
        public JsonResult GetYear(int CourseID)
        {
            var co = from c in cmrs.CoursePerYears where c.CourseID == CourseID select c;
            List<int> stateList = new List<int>();
            List<int> stateMoveList = new List<int>();
            for (int i = 2016
                ; i < 2050; i++)
            {
                stateList.Add(i);
                stateMoveList.Add(i);
            }

            foreach (var a in stateList)
            {
                foreach (var c in co)
                {
                    if (a == Int32.Parse(c.Year))
                    {
                        stateMoveList.Remove(a);
                    }
                }
            }
            return Json(stateMoveList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddNew()
        {
            var cl = (from c in cmrs.AccountRoles
                      join
                          cc in cmrs.Accounts on c.AccountID equals cc.ID
                      where c.RoleName.Trim().Equals("CL") && cc.Locked == false
                      select cc).ToArray();
            ViewData["cl"] = cl;
            var cm = (from c in cmrs.AccountRoles
                      join
                          cc in cmrs.Accounts on c.AccountID equals cc.ID
                      where c.RoleName.Trim().Equals("CM") && cc.Locked == false
                      select cc).ToArray();
            ViewData["cm"] = cm;
            var co = (from c in cmrs.Courses
                      where c.CourseStatus.Equals("1")
                      select c).ToArray();
            ViewData["co"] = co;
            return View("AddCPY");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNewCPY(FormCollection collection)
        {
            List<CPY> lst = new List<CPY>();

            int course_id = Int32.Parse(collection["cbxCourseName"]);
            if (course_id == 0)
            {

                TempData["MessageError"] = "Please chose Course Name  !";
                return RedirectToAction("AddNew", "CoursePerYear");
            }
            var co = (from c in cmrs.Courses where c.ID == course_id select c).FirstOrDefault();
            string strname = co.CourseCode;
            string stryear = collection["cbxYear"];
            if (stryear.Trim().Equals("0"))
            {
                TempData["MessageError"] = "Please chose Year !";
                return RedirectToAction("AddNew", "CoursePerYear");
            }
            int cl_id = Int32.Parse(collection["cbxCL"]);
            int cm_id = Int32.Parse(collection["cbxCM"]);
            string status = collection["stt"];
            string cpy_code = strname + "_" + stryear;
            DateTime create_time = DateTime.Now;
            var cpy = new CoursePerYear();
            cpy.CL_ID = cl_id;
            cpy.CM_ID = cm_id;
            cpy.CourseID = course_id;
            cpy.CPY_Code = cpy_code;
            cpy.Create_Time = create_time;
            cpy.Year = stryear;
            cpy.Status = Int32.Parse(status);
            try
            {

                cmrs.CoursePerYears.Add(cpy);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Success !";
                return RedirectToAction("Index", "CoursePerYear");

            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
        }

        public ActionResult DeleteCPY(int id)
        {
            //int variable =Int32.Parse( Request.Params["variable"]);
            var cpy = (from data in cmrs.CoursePerYears
                       where data.ID == id
                       select data).FirstOrDefault();
            try
            {


                cmrs.CoursePerYears.Remove(cpy);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "Error!";
                throw new Exception(ex.Message);
            }
            return RedirectToAction("index", "CoursePerYear");
        }
        public ActionResult GotoUpdate(int id)
        {

            var cpy = (from data in cmrs.CoursePerYears
                       where data.ID == id
                       select
                       data).FirstOrDefault();

            var cl = (from c in cmrs.AccountRoles
                      join
                          cc in cmrs.Accounts on c.AccountID equals cc.ID
                      where c.RoleName.Trim().Equals("CL") && cc.Locked == false
                      select cc).ToArray();
            ViewData["cl"] = cl;
            var cm = (from c in cmrs.AccountRoles
                      join
                          cc in cmrs.Accounts on c.AccountID equals cc.ID
                      where c.RoleName.Trim().Equals("CM") && cc.Locked == false
                      select cc).ToArray();
            ViewData["cm"] = cm;
            var co = (from c in cmrs.Courses
                      where c.CourseStatus.Equals("1")
                      select c).ToArray();
            ViewData["co"] = co;

            return View("EditCPY", cpy);

        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateCPY(FormCollection collection)
        {
            int page = Int32.Parse(collection["page"].Trim());
            int id = Int32.Parse(collection["idcpy"].Trim());
            var cp = (from data in cmrs.CoursePerYears

                      where data.ID == id
                      select data).FirstOrDefault();

            //List<CPY> lst = new List<CPY>();
            //lst = getAllCPY();
            //var re = new CPY();
            //foreach (var data in lst)
            //{
            //    if (data.ID == cp.ID)
            //    {
            //        re = data;
            //    }
            //}
            //lst.Remove(re);
            string course_name = collection["cbxCourseName"];
            var co = (from c in cmrs.Courses where c.CourseName == course_name select c).FirstOrDefault();
            string strname = co.CourseCode;
            string stryear = collection["cbxYear"];
            int cl_id = Int32.Parse(collection["cbxCL"]);
            int cm_id = Int32.Parse(collection["cbxCM"]);
            string status = collection["stt"];
            string cpy_code = strname + "_" + stryear;
            // DateTime create_time = DateTime.Now;

            cp.CL_ID = cl_id;
            cp.CM_ID = cm_id;
            cp.CourseID = co.ID;
            cp.CPY_Code = cpy_code;
            // cp.Create_Time = create_time;
            cp.Year = stryear;
            cp.Status = Int32.Parse(status);

            //var count = 0;
            //foreach (var data in lst)
            //{
            //    if ((data.CourseName.ToLower()).Equals(cpy_code
            //        .ToLower()))
            //    {
            //        count = 1;
            //    }

            //}
            try
            {
                //if (count == 0)
                //{

                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Success !";
                return RedirectToAction("ManageCPYList", "CoursePerYear", new { PageNum = page });
                //}
                //else
                //{
                //    TempData["MessageError"] = "Error name exist!";
                //    return RedirectToAction("ManageCPYList", "CoursePerYear", new { PageNum = page });
                //}
            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
        }
        [HttpPost]
        public ActionResult SearchCPY(int pagenum, string key, FormCollection collection)
        {
            key = collection["search_key_news"].Trim();
            //   string keynews = Request.Params["search_key_news"];
            if (key != null)
            {
                ViewBag.countSearch = getAllSearchCPY(key).Count;
                //ViewBag.search_key_news = strSearch;
                return View("SearchCPY", padingSearch(PageSize, pagenum, key));
            }
            return View();
        }

        [HttpGet]
        public ActionResult SearchCPY(int pagenum, string key)
        {
            key = Request.QueryString["search_key_news"].Trim();
            //   string keynews = Request.Params["search_key_news"];
            if (key != null)
            {
                ViewBag.countSearch = getAllSearchCPY(key).Count;
                //ViewBag.search_key_news = strSearch;
                return View("SearchCPY", padingSearch(PageSize, pagenum, key));
            }
            return View();
        }
        public List<CPY> getAllSearchCPY(string search)
        {
            List<CPY> lst = new List<CPY>();
            var query = (from data in cmrs.Courses
                         join
                             dat in cmrs.CoursePerYears on data.ID equals dat.CourseID
                         join
                             da in cmrs.Accounts on dat.CM_ID equals da.ID
                         join
                             d in cmrs.Accounts on dat.CL_ID equals d.ID
                         where data.CourseName.Trim().ToLower().Contains(search.Trim().ToLower()) || dat.CPY_Code.Trim().ToLower().Contains(search.Trim().ToLower()) || dat.Year.ToString().ToLower().Contains(search.Trim().ToLower()) || d.FullName.Trim().ToLower().Contains(search.Trim().ToLower()) || da.FullName.Trim().ToLower().Contains(search.Trim().ToLower())
                         select new
                         {
                             ID = dat.ID,
                             CL_ID = dat.CL_ID,
                             CM_ID = dat.CM_ID,
                             Create_Time = dat.Create_Time,
                             CourseID = dat.CourseID,
                             CPY_Code = dat.CPY_Code,
                             Status = dat.Status,
                             Year = dat.Year,
                             CourseName = data.CourseName,
                             CM_FullName = da.FullName,
                             CL_FullName = d.FullName

                         }).Distinct().OrderByDescending(x => x.Create_Time);
            foreach (var item in query)
            {
                CPY cpy = new CPY();
                cpy.ID = item.ID;
                cpy.Status = item.Status;
                cpy.Year = item.Year;
                cpy.CM_Name = item.CM_FullName;
                cpy.CourseName = item.CourseName;
                cpy.CPY_Code = item.CPY_Code;
                cpy.CL_Name = item.CL_FullName;
                lst.Add(cpy);
            }
            return lst;
        }

        public List<CPY> padingSearch(int pagesize, int pagenum, string search)
        {
            ViewBag.search_key_news = search;
            var lst = getAllSearchCPY(search.Trim());
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            return list;
        }

    }
}
