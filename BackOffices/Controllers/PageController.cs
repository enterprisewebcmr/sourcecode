﻿using BackOffices.CustomFilters;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.WrapModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffices.Controllers
{
    public class PageController : Controller
    {
        //
        // GET: /Page/
        private readonly IPageImpl _iPageImpl;
        private readonly IMenuImpl _iMenuImpl;
        private readonly IMenuPageImpl _iMenuPageImpl;
        private readonly IPageRoleImpl _iPageRoleImpl;
        private readonly IRoleImpl _iRoleImpl;

        private readonly IPagePermissionImpl _iPagePermissionImpl;
        private readonly IPagePermissionRoleImpl _iPagePermissionRoleImpl;

        public PageController(IPageImpl iPageImpl, IMenuImpl iMenuImpl, IMenuPageImpl iMenuPageImpl, 
            IPageRoleImpl iPageRoleImpl, IRoleImpl iRoleImpl, 
            IPagePermissionImpl iPagePermissionImpl, IPagePermissionRoleImpl iPagePermissionRoleImpl)
        {
            _iPageImpl = iPageImpl;
            _iMenuImpl = iMenuImpl;
            _iMenuPageImpl = iMenuPageImpl;
            _iPageRoleImpl = iPageRoleImpl;
            _iRoleImpl = iRoleImpl;
            _iPagePermissionImpl = iPagePermissionImpl;
            _iPagePermissionRoleImpl = iPagePermissionRoleImpl;
        }

       [ValidateUserRoles]
        public ActionResult Index()
        {
            try
            {
                var wrapPages = new List<WrapPage>();
                var parentPages = _iPageImpl.GetByParentID(0).OrderBy(m => m.PageName).ToList();
                foreach (var item in parentPages)
                {
                    var wrapPage = new WrapPage
                    {
                        Page = item,
                        SubPages = _iPageImpl.GetByParentID(item.ID).OrderBy(sp => sp.PageName).ToList()
                    };
                    wrapPages.Add(wrapPage);
                }

                return View(wrapPages);
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return View(new List<WrapPage>());
        }

       [ValidateUserRoles]
       public ActionResult Add()
       {
           ViewBag.ParentPages = _iPageImpl.GetByParentID(0);
           ViewBag.Menus = _iMenuImpl.All();
           ViewBag.Roles = _iRoleImpl.All();
           return View("Edit", new Page());
       }
       [ValidateUserRoles]
       public ActionResult Edit(int pageid)
       {

           try
           {
               var _page = _iPageImpl.GetById(pageid);
               ViewBag.ParentPages = _iPageImpl.GetByParentID(0);
               ViewBag.PageRoles = _iPageRoleImpl.GetByPageID(pageid).ToList();
               ViewBag.Menus = _iMenuImpl.All();
               ViewBag.Roles = _iRoleImpl.All();
               if (_page == null)
               {
                   ViewBag.MenuPages = _iMenuPageImpl.GetMenuPageByPageID(pageid).ToList();
                   TempData["MessageError"] = "Không tồn tại trang này.";
               }
               else
               {
                   ViewBag.MenuPages = _iMenuPageImpl.GetMenuPageByPageID(pageid).ToList();
                   return View(_page);
               }
           }
           catch (Exception ex)
           {
               TempData["MessageError"] = ex.InnerException;
           }

           return View(new Page());
       }


       [HttpPost]
       public ActionResult Edit(Page model, int slPage, string[] tagmenu_selected, string[] tagrole_selected)
       {
           try
           {

               if (model.ID > 0)
               {
                   //Edit Page
                   var _page = _iPageImpl.GetById(model.ID);
                   _page.PageName = model.PageName;

                   if (model.PageURL.Substring(model.PageURL.Length - 1) == "/")
                   {
                       _page.PageURL = model.PageURL.Substring(0, model.PageURL.LastIndexOf("/"));
                   }
                   else
                   {
                       _page.PageURL = model.PageURL;
                   }

                   _page.ParentPageID = slPage;
                   _iPageImpl.Update(_page);
                   if (_page.ID > 0)
                   {
                       //edit menupage follow tag
                       _iMenuPageImpl.DeleteAllByPageID(_page.ID);
                       if (tagmenu_selected != null)
                       {
                           foreach (var menuid in tagmenu_selected)
                           {
                               var _menu = _iMenuImpl.GetById(Convert.ToInt32(menuid));
                               if (_menu != null)
                               {
                                   MenuPage _MenuPage = new MenuPage();
                                   _MenuPage.MenuID = _menu.ID;
                                   _MenuPage.MenuName = _menu.MenuName;
                                   _MenuPage.PageID = _page.ID;
                                   _MenuPage.PageName = _page.PageName;
                                   _MenuPage.PageURL = _page.PageURL;
                                   _iMenuPageImpl.Add(_MenuPage);
                               }
                           }
                       }
                       //#################################################
                       //tag role ################################
                       _iPageRoleImpl.DeleteAllByPageID(model.ID);
                       if (tagrole_selected != null)
                       {
                           //tag_selected = tag_selected.Union(tag_selected).ToArray();

                           foreach (var item in tagrole_selected)
                           {
                               string[] _strSplit = item.Split(new char[] { '|' });
                               PageRole _PageRole = new PageRole();
                               _PageRole.PageID = _page.ID;
                               _PageRole.PageName = _page.PageName;
                               _PageRole.RoleID = Convert.ToInt32(_strSplit[0]);
                               _PageRole.RoleName = _strSplit[1];
                               _iPageRoleImpl.Add(_PageRole);
                           }
                       }
                       //#########################################


                       TempData["MessageSuccess"] = "Thay đổi trang \"" + _page.PageName + "\" thành công.";
                       return RedirectToAction("Edit", new { pageid = _page.ID });
                   }

               }
               else
               {
                   //Add Page
                   Page _page = new Page();
                   _page.PageName = model.PageName;

                   if (model.PageURL.Substring(model.PageURL.Length - 1) == "/")
                   {
                       _page.PageURL = model.PageURL.Substring(0, model.PageURL.LastIndexOf("/"));
                   }
                   else
                   {
                       _page.PageURL = model.PageURL;
                   }


                   _page.ParentPageID = slPage;
                   _iPageImpl.Add(_page);
                   if (_page.ID > 0)
                   {
                       TempData["MessageSuccess"] = "Thêm mới trang \"" + _page.PageName + "\" thành công.";

                       //add menupage follow tag #########################
                       if (tagmenu_selected != null)
                       {
                           foreach (var menuid in tagmenu_selected)
                           {
                               var _menu = _iMenuImpl.GetById(Convert.ToInt32(menuid));
                               if (_menu != null)
                               {
                                   MenuPage _MenuPage = new MenuPage();
                                   _MenuPage.MenuID = _menu.ID;
                                   _MenuPage.MenuName = _menu.MenuName;
                                   _MenuPage.PageID = _page.ID;
                                   _MenuPage.PageName = _page.PageName;
                                   _MenuPage.PageURL = _page.PageURL;
                                   _iMenuPageImpl.Add(_MenuPage);
                               }
                           }
                       }
                       //############################################

                       //tag role ################################
                       if (tagrole_selected != null)
                       {
                           //tag_selected = tag_selected.Union(tag_selected).ToArray();

                           foreach (var item in tagrole_selected)
                           {
                               string[] _strSplit = item.Split(new char[] { '|' });
                               PageRole _PageRole = new PageRole();
                               _PageRole.PageID = _page.ID;
                               _PageRole.PageName = _page.PageName;
                               _PageRole.RoleID = Convert.ToInt32(_strSplit[0]);
                               _PageRole.RoleName = _strSplit[1];
                               _iPageRoleImpl.Add(_PageRole);
                           }
                       }
                       //#########################################


                       return RedirectToAction("Add");
                   }
               }


             


           }
           catch (Exception ex)
           {
               TempData["MessageError"] = ex.InnerException;
           }
           return View(model);
       }





       [Authorize]
       [HttpPost]
       public string Delete(int pageid)
       {
           try
           {
               //xoa
               var _page = _iPageImpl.GetById(pageid);

               if (_page.ParentPageID == 0)
               {
                   List<Page> _subPages = _iPageImpl.GetByParentID(_page.ID).ToList();
                   foreach (var item in _subPages)
                   {
                       _iPageRoleImpl.DeleteAllByPageID(item.ID);
                       _iPagePermissionImpl.DeleteAllPagePermissionByPageID(item.ID);
                       _iPagePermissionRoleImpl.DeleteAllByPageID(item.ID);

                       _iMenuPageImpl.DeleteAllByPageID(item.ID);
                       

                       _iPageImpl.Delete(item);

                   }
                   _iMenuPageImpl.DeleteAllByPageID(_page.ID);

               }
               else
               {
                   _iPageRoleImpl.DeleteAllByPageID(_page.ID);
                   _iPagePermissionImpl.DeleteAllPagePermissionByPageID(_page.ID);
                   _iPagePermissionRoleImpl.DeleteAllByPageID(_page.ID);
                   _iMenuPageImpl.DeleteAllByPageID(_page.ID);

               }

               _iPageImpl.Delete(_page);

               return "Xóa trang \"" + _page.PageName + "\" thành công.";
           }
           catch { }
           return "FAIL";
       }




    }
}
