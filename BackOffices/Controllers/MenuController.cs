﻿using BackOffices.CustomFilters;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.WrapModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffices.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/

        private readonly IMenuImpl _iMenuImpl;
        private readonly IMenuPageImpl _iMenuPageImpl;
        public MenuController(IMenuImpl iMenuImpl, IMenuPageImpl iMenuPageImpl) {
            _iMenuImpl = iMenuImpl;
            _iMenuPageImpl = iMenuPageImpl;
        }


        [ValidateUserRoles]
        public ActionResult Index()
        {
            try
            {
                var resultMenus = _iMenuImpl.All();
                if (resultMenus != null)
                {
                    List<WrapMenu> _WrapMenus = new List<WrapMenu>();
                    foreach (var menu in resultMenus)
                    {
                        WrapMenu _WrapMenu = new WrapMenu();
                        var _pages = _iMenuPageImpl.GetPageByMenuID(menu.ID);
                        _WrapMenu.Menu = menu;
                        _WrapMenu.Pages = _pages == null ? null : _pages.ToList();
                        _WrapMenus.Add(_WrapMenu);
                    }

                    return View(_WrapMenus);
                }
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return View(new List<WrapMenu>());
        }

        [Authorize]
        [HttpPost]
        public string Edit(int menuid, string menuname) {
            try {
                var _menu = _iMenuImpl.GetById(menuid);
                _menu.MenuName = menuname;
                _iMenuImpl.Update(_menu);


                var menupages = _iMenuPageImpl.GetMenuPageByMenuID(menuid).ToList();
                foreach (var item in menupages)
                {
                    item.MenuName = _menu.MenuName;
                    _iMenuPageImpl.Update(item);
                }

                return "Thay đổi \"" + _menu.MenuName + "\" Menu thành công.";
            }
            catch { }
            return "FAIL";
        }

        [Authorize]
        [HttpPost]
        public string Add(string menuname)
        {
            try
            {
                Menu _menu = new Menu();
                _menu.MenuName = menuname;
                _iMenuImpl.Add(_menu);
                if (_menu.ID > 0)
                {
                    return _menu.ID.ToString();
                    
                }
            }
            catch { }
            return "FAIL";
        }

        [Authorize]
        [HttpPost]
        public string Delete(int menuid)
        {
            try
            {
                var _menu = _iMenuImpl.GetById(menuid);

                _iMenuImpl.Delete(_menu);
                _iMenuPageImpl.DeleteAllByMenuID(menuid);

                return "Xóa \"" + _menu.MenuName + "\" Menu thành công.";
            }
            catch { }
            return "FAIL";
        }

    }
}
