﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Dal;
using BackOffices.Models;
using System.Globalization;

namespace BackOffices.Controllers
{
    public class FacultyController : Controller
    {

        Dal.CMRSecurityEntities cmrs = new CMRSecurityEntities();
        private int PageSize = 5;
        //
        // GET: /Faculty/

        public ActionResult Index()
        {
            //List<Faculty> lst = new List<Faculty>();
            //lst = getAllFa();
            return RedirectToAction("ManageFacultyList", "Faculty", new { PageNum = 1 });
        }

        public ActionResult ManageFacultyList(int pagenum)
        {

            //pagenum = 1;
            List<Faculties> lst = new List<Faculties>();

            lst = getAllFa();
            ViewBag.countnewsall = countAll();
            //  ViewBag.count = lst.Count().ToString().Trim();



            return View("index", pading(PageSize, pagenum));
        }
        public List<Faculties> pading(int pagesize, int pagenum)
        {

            // status = Int32.Parse(Request.Params["variable"]);

            var lst = getAllFa(); ;
            // ViewBag.count = lst.Count().ToString().Trim();
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            return list;
        }
        public int countAll()
        {
            var query = from data in cmrs.Faculties
                        select data;
            return query.Count();
        }
        public List<Faculties> getAllFa()
        {
            List<Faculties> lst = new List<Faculties>();
            var query = from data in cmrs.Faculties
                        join
                            dat in cmrs.Accounts on data.DLT_ID equals dat.ID
                        orderby data.Create_Time descending
                        select new
                        {
                            data.ID,
                            data.FaName,
                            data.DLT_ID,
                            data.Create_Time,
                            dat.FullName
                        };
            foreach (var item in query)
            {
                Faculties fa = new Faculties();
                fa.ID = item.ID;
                fa.FaName = item.FaName;
                fa.DLT_ID = item.DLT_ID;
                fa.DLTName = item.FullName;
                fa.Create_Time = item.Create_Time;
                lst.Add(fa);
            }
            return lst;
        }
        public ActionResult AddNew()
        {
            var cat = (from c in cmrs.AccountRoles join cc in cmrs.Accounts on c.AccountID equals cc.ID where c.RoleName.Trim().Equals("DLT") select cc).ToArray();
            ViewData["a"] = cat;
            return View("AddNew");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNewFa(FormCollection collection)
        {
            List<Faculties> lst = new List<Faculties>();
            lst = getAllFa();

            string strname = collection["Name"];
            int roletag = Int32.Parse(collection["combobox"]);
            var fa = new Faculty();
            fa.FaName = strname;
            fa.DLT_ID = roletag;
            fa.Create_Time = DateTime.Now;
            var count = 0;
            foreach (var data in lst)
            {
                if ((data.FaName.ToLower()).Equals(strname.ToLower()))
                {

                    count = 1;
                }

            }
            try
            {
                if (count == 0)
                {
                    cmrs.Faculties.Add(fa);
                    cmrs.SaveChanges();
                    TempData["MessageSuccess"] = "Success !";
                    return RedirectToAction("index", "faculty");
                }
                else
                {
                    TempData["MessageError"] = "Error name exist!";
                    return RedirectToAction("index", "faculty");
                }
            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
        }

        public ActionResult DeleteFa(int id)
        {
            //int variable =Int32.Parse( Request.Params["variable"]);

            var fa = (from data in cmrs.Faculties
                      where data.ID == id
                      select data).FirstOrDefault();
            try
            {


                cmrs.Faculties.Remove(fa);
                cmrs.SaveChanges();
                TempData["MessageSuccess"] = "Done !";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = "Error!";
                throw new Exception(ex.Message);
            }
            return RedirectToAction("index", "faculty");
        }
        public ActionResult GotoUpdate(int id)
        {

            var query = (from data in cmrs.Faculties
                         where data.ID == id
                         select data).FirstOrDefault();
            var cat = (from c in cmrs.AccountRoles where c.RoleName.Trim().Equals("DLT") select c).ToArray();

            ViewData["a"] = cat;


            return View("Edit", query);

        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateFa(FormCollection collection)
        {
            int page = Int32.Parse(collection["page"].Trim());
            int id = Int32.Parse(collection["idfa"].Trim());
            var fa = (from data in cmrs.Faculties

                      where data.ID == id
                      select data).FirstOrDefault();

            List<Faculties> lst = new List<Faculties>();
            lst = getAllFa();
            var re = new Faculties();
            foreach (var data in lst)
            {
                if (data.ID == fa.ID)
                {
                    re = data;
                }
            }
            lst.Remove(re);
            string strname = collection["Name"];
            int roletag = Int32.Parse(collection["combobox"]);
            fa.FaName = strname;
            fa.DLT_ID = roletag;

            var count = 0;
            foreach (var data in lst)
            {
                if ((data.FaName.ToLower()).Equals(strname.ToLower()))
                {
                    count = 1;
                }

            }
            try
            {
                if (count == 0)
                {

                    cmrs.SaveChanges();
                    TempData["MessageSuccess"] = "Success !";
                    return RedirectToAction("ManageFacultyList", "Faculty", new { PageNum = page });
                }
                else
                {
                    TempData["MessageError"] = "Error name exist!";
                    return RedirectToAction("ManageFacultyList", "Faculty", new { PageNum = page });
                }
            }
            catch (Exception e)
            {
                TempData["MessageError"] = "Error !";
                throw new Exception(e.Message);
            }
        }
        [HttpPost]
        public ActionResult SearchFaculty(int pagenum, string key, FormCollection collection)
        {
            key = collection["search_key_news"].Trim();
            //   string keynews = Request.Params["search_key_news"];
            if (key != null)
            {
                ViewBag.countSearch = getAllSearch(key).Count;
                //ViewBag.search_key_news = strSearch;
                return View("Search", padingSearch(PageSize, pagenum, key));
            }
            return View();
        }

        [HttpGet]
        public ActionResult SearchFaculty(int pagenum, string key)
        {
            key = Request.QueryString["search_key_news"].Trim();
            //   string keynews = Request.Params["search_key_news"];
            if (key != null)
            {
                ViewBag.countSearch = getAllSearch(key).Count;
                //ViewBag.search_key_news = strSearch;
                return View("Search", padingSearch(PageSize, pagenum, key));
            }
            return View();
        }
        public List<Faculties> getAllSearch(string search)
        {
            List<Faculties> lst = new List<Faculties>();
            var query = from data in cmrs.Faculties
                        where (data.FaName.ToLower().Contains(search.Trim().ToLower()))
                        select new
                        {
                            data.ID,
                            data.FaName,
                            data.DLT_ID
                        };
            foreach (var item in query)
            {
                Faculties fa = new Faculties();
                fa.ID = item.ID;
                fa.FaName = item.FaName;
                fa.DLT_ID = item.DLT_ID;
                lst.Add(fa);
            }

            return lst;
        }

        public List<Faculties> padingSearch(int pagesize, int pagenum, string search)
        {
            ViewBag.search_key_news = search;
            var lst = getAllSearch(search.Trim());
            var list = lst.Skip(pagesize * (pagenum - 1)).Take(pagesize).ToList();
            return list;
        }
    }
}
