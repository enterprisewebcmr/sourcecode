﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Security.Foundation.Data.Models;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Implements;
using BackOffices.CustomFilters;


namespace BackOffices.Controllers
{
    public class RoleController : Controller
    {
        //
        // GET: /Role/
        private IRoleImpl _IRoleImpl;
        private IAccountRoleImpl _IAccountRoleImpl;
        private IPageRoleImpl _IPageRoleImpl;
        private IPagePermissionRoleImpl _IPagePermissionRoleImpl;
        private IMenuPageImpl _IMenuPageImpl;
        public RoleController(IRoleImpl __IRoleImpl,
            IAccountRoleImpl __IAccountRoleImpl,
            IPageRoleImpl __IPageRoleImpl,
            IPagePermissionRoleImpl __IPagePermissionRoleImpl,
            IMenuPageImpl __IMenuPageImpl)
        {
            _IRoleImpl = __IRoleImpl;
            _IAccountRoleImpl = __IAccountRoleImpl;
            _IPageRoleImpl = __IPageRoleImpl;
            _IPagePermissionRoleImpl = __IPagePermissionRoleImpl;
            _IMenuPageImpl = __IMenuPageImpl;
        }

        [ValidateUserRoles]
        public ActionResult Index()
        {
            var results = _IRoleImpl.All();
            return View(results);
        }


        public ActionResult JsonTagRole()
        {
            var results = _IRoleImpl.All();
            return Json(results.Select(r => new { label = r.RoleName, value = r.ID + "|" + r.RoleName }), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public string Add(string rolename)
        {
            //add role
            Role _role = new Role { RoleName = rolename };
            _IRoleImpl.Add(_role);
            if (_role.ID > 0)
            {
                return _role.ID.ToString();
            }
            return "Thêm mới role \"" + rolename + "\" FAIL";
        }

        
        [Authorize]
        [HttpPost]
        public string Edit(int roleid, string rolename)
        {
            try
            {
                // edit role
                Role _role = _IRoleImpl.GetById(roleid);
                _role.RoleName = rolename;
                _IRoleImpl.Update(_role);


                var _AccountRoles = _IAccountRoleImpl.GetByRoleID(roleid).ToList();
                foreach (var item in _AccountRoles)
                {
                    item.RoleName = _role.RoleName;
                    _IAccountRoleImpl.Update(item);
                }

                var _PageRoles = _IPageRoleImpl.GetbyRoleID(roleid).ToList();
                foreach (var item in _PageRoles)
                {
                    item.RoleName = _role.RoleName;
                    _IPageRoleImpl.Update(item);
                }

                var _PagePermissionRoles = _IPagePermissionRoleImpl.GetByRoleID(roleid).ToList();
                foreach (var item in _PagePermissionRoles)
                {
                    item.RoleName = _role.RoleName;
                    _IPagePermissionRoleImpl.Update(item);
                }

                

                return "Thay đổi role thành công";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return "Thay đổi role FAIL";
        }

        [Authorize]
        [HttpPost]
        public string Delete(int roleid)
        {
            try
            {
                // edit role
                Role _role = _IRoleImpl.GetById(roleid);
                

                var _AccountRoles = _IAccountRoleImpl.GetByRoleID(roleid).ToList();
                foreach (var item in _AccountRoles)
                {
                    
                    _IAccountRoleImpl.Delete(item);
                }

                var _PageRoles = _IPageRoleImpl.GetbyRoleID(roleid).ToList();
                foreach (var item in _PageRoles)
                {

                    _IMenuPageImpl.DeleteAllByPageID(item.PageID);
                    _IPageRoleImpl.Delete(item);
                }

                var _PagePermissionRoles = _IPagePermissionRoleImpl.GetByRoleID(roleid).ToList();
                foreach (var item in _PagePermissionRoles)
                {
                    _IPagePermissionRoleImpl.Delete(item);
                }

                _IRoleImpl.Delete(_role);

                return "Delete role thành công";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return "Delete role FAIL";
        }


    }
}
