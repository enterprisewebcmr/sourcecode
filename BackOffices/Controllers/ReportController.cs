﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackOffices.Dal;
using BackOffices.Models;
using System.Web.Helpers;

namespace BackOffices.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/
        public List<Report> listReport = null;
        CMRSecurityEntities cmrs = new CMRSecurityEntities();
        List<OutComes> listOutcome = new List<OutComes>();
        int OutComeCount = 0;
        int CPY_ID;
        public ActionResult Index()
        {
            
            return View("Report");
        }
        public ActionResult Show(int CPY_ID) {
           // var query = from;
           
            return RedirectToAction("Index", showReport(CPY_ID));
        }
        public void GetOutcomesList(int cpyID)
        {
            
            var query = (from data in cmrs.OutComes

                         where data.CoupID == cpyID
                         select new
                         {
                             ID = data.ID,
                             OC_name = data.OutcomeName,
                             Percent = data.Percentage,
                             CPY_ID = data.CoupID

                         });

            foreach (var item in query)
            {
                OutComes oc = new OutComes();
                oc.ID = item.ID;
                oc.CPY_ID = item.CPY_ID;
                oc.OutCome_Name = item.OC_name;
                oc.Percent = item.Percent;
                listOutcome.Add(oc);
            }
            OutComeCount = listOutcome.Count();
            List< int[] > studentlist = new List<int[]>();

            
        }
        public ActionResult GetOverralPieChart()
        {
            List<int> stuidList = new List<int>();
            List<float> overralList = new List<float>();
            
            
            var query = (from data in cmrs.CoursePerYears
                         join oc in cmrs.OutComes on data.CourseID equals oc.CoupID
                         join ts in cmrs.Transcripts on oc.ID equals ts.OC_ID

                         where data.CourseID == CPY_ID
                         select new
                         {
                             ID=ts.StudentID,
                             Score=ts.Score,
                             Percen=oc.Percentage,
                         });

            foreach (var item in query)
            {
                int? stuID = item.ID; int stuidFix = stuID.GetValueOrDefault();
                int? score = item.Score;
                float scorenew = Convert.ToSingle( item.Percen * score/100);
                int index = stuidList.IndexOf(stuidFix);
                    if(index>=0){
                    overralList[index] = overralList[index] + scorenew;
                   
                }
                else
                {
                    stuidList.Add(stuidFix);
                    overralList.Add(scorenew);
                }
            }
            int sc10 = 10;
            int sc20 = 10;
            int sc30 = 10;
            int sc40 = 10;
            int sc50 = 10;
            int sc60 = 20;
            int sc70 = 20;
            int sc80 = 10;
            int sc90 = 10;
            int sc100 = 30;
            int size = overralList.Count;
            for(int x=0;x< size; x++)
            {
                float scorecur = overralList[x];
                if (x < 10f)
                {
                    sc10++;
                }
               
                if (x < 20f&&x>=10f)
                {
                    sc20++;
                }

                if (x < 30 && x >= 20f)
                {
                    sc30++;
                }

                if (x < 40f && x >= 30f)
                {
                    sc40++;
                }
                if (x < 50f && x >=40f)
                {
                    sc50++;
                }
                if (x < 60f && x >= 50f)
                {
                    sc60++;
                }
                if (x < 70f && x >= 60f)
                {
                    sc70++;
                }
                if (x < 80f && x >= 70f)
                {
                    sc80++;
                }
                if (x < 90f && x >= 80f)
                {
                    sc90++;
                }
                else
                {
                    sc100++;
                }

            }

            var chart = new Chart(width: 600, height: 400)
                .AddSeries(
                            chartType: "pie",
                            xValue: new[] { "0-10", "10-20","20-30", "30-40", "40-50","50=60","60-70","70-80","80-90","90-100" },
                            yValues: new[] { sc10.ToString(), sc20.ToString(), sc30.ToString(), sc40.ToString(), sc50.ToString(), sc60.ToString()
                            , sc70.ToString(), sc80.ToString(), sc90.ToString(), sc100.ToString() })
                            .GetBytes("png");
            return File(chart, "image/bytes");
        }
        public List<Report> showReport(int cpyid)
        {
            
            return listReport;
        }
        public ActionResult DrawChart()
        {
            var chart = new Chart(width: 300, height: 200)
                .AddSeries(
                            chartType: "column",
                            xValue: new[] { "10 Records", "20 Records", "30 Records", "40 Records" },
                            yValues: new[] { "50", "60", "78", "80",  })
                            .GetBytes("png");
            return File(chart, "image/bytes");
        }
    }
}
