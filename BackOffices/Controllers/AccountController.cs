﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using Security.Foundation.Data;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.Implements;
using Security.Foundation.Data.Interfaces;


using Security.Foundation.Utility;

using System.Web.Security;
using BackOffices.CustomFilters;
using Security.Foundation.Data.WrapModels;
using System.Web.Script.Serialization;

namespace BackOffices.Controllers
{
    public class AccountController : Controller
    {
        private IAccountImpl _IAccountImpl;
        private IAccountRoleImpl _IAccountRoleImpl;
        private IRoleImpl _IRoleImpl;

        public AccountController(IAccountImpl __IAccountImpl, IAccountRoleImpl __IAccountRoleImpl, IRoleImpl __IRoleImpl)
        {
            _IAccountImpl = __IAccountImpl;
            _IAccountRoleImpl = __IAccountRoleImpl;
            _IRoleImpl = __IRoleImpl;
        }

        [ValidateUserRoles]
        public ActionResult Index()
        {
            var results = _IAccountImpl.All();
            return View(results);
        }

        [ValidateUserRoles]
        public ActionResult Add()
        {
            ViewBag.Roles = _IRoleImpl.All();
            return View("Edit", new Account());
        }

        [ValidateUserRoles]
        public ActionResult Edit(int accountid = -1)
        {
            try
            {
                var _account = _IAccountImpl.GetById(accountid);
                ViewBag.AccountRoles = _IAccountRoleImpl.GetByAccountID(accountid).ToList();
                ViewBag.Roles = _IRoleImpl.All();
                if (_account == null)
                {
                    TempData["MessageError"] = "Không tồn tại thành viên";
                }
                else
                {
                    return View(_account);
                }
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return View(new Account());
        }

        [HttpPost]
        public ActionResult Edit(Account model, string[] roletag, int? Locked, int? SupperAdmin)
        {
           
                try
                {
                    if (model.ID > 0)
                    {
                        //############### Edit Account #############
                        Account _Account = _IAccountImpl.GetById(model.ID);
                        _Account.Email = model.Email;
                        _Account.FullName = model.FullName;
                        _Account.UserName = model.UserName.ToLower();
                        _Account.Locked = Locked==1?true:false;
                        _Account.SupperAdmin = SupperAdmin==1?true:false;
                        //_Account.Password = AccountCrypto.Encrypt(model.Password, model.UserName);
                        _IAccountImpl.Update(_Account);

                        if (_Account.ID > 0)
                        {
                            TempData["MessageSuccess"] = "Thay đổi thành viên \"" + _Account.UserName + "\" thành công.";
                            //tag role ################################
                            _IAccountRoleImpl.DeleteAllByAccountID(model.ID);
                            if (roletag != null)
                            {
                                //tag_selected = tag_selected.Union(tag_selected).ToArray();

                                foreach (var item in roletag)
                                {
                                    string[] _strSplit = item.Split(new char[] { '|' });
                                    AccountRole _AccountRole = new AccountRole();
                                    _AccountRole.AccountID = _Account.ID;
                                    _AccountRole.AccountName = _Account.UserName;
                                    _AccountRole.RoleID = Convert.ToInt32(_strSplit[0]);
                                    _AccountRole.RoleName = _strSplit[1];
                                    _IAccountRoleImpl.Add(_AccountRole);
                                }
                            }
                            //#########################################
                            return RedirectToAction("Edit", "Account", new { accountid = _Account.ID });
                        }



                    }
                    else
                    {
                        //############### Insert Account ###########


                        Account _Account = new Account();
                        _Account.Email = model.Email;
                        _Account.FullName = model.FullName;
                        _Account.UserName = model.UserName.ToLower();
                        _Account.Locked = Locked == 1 ? true : false;
                        _Account.SupperAdmin = SupperAdmin == 1 ? true : false;

                        _Account.Password = AccountCrypto.Encrypt(model.Password, _Account.UserName);
                        _Account.CreatedDate = DateTime.Now;
                        _IAccountImpl.Add(_Account);
                        if (_Account.ID > 0)
                        {
                            TempData["MessageSuccess"] = "Thêm thành viên \"" + _Account.UserName + "\" thành công.";

                            //tag role ################################
                            if (roletag != null)
                            {
                                //tag_selected = tag_selected.Union(tag_selected).ToArray();

                                foreach (var item in roletag)
                                {
                                    string[] _strSplit = item.Split(new char[] { '|' });
                                    AccountRole _AccountRole = new AccountRole();
                                    _AccountRole.AccountID = _Account.ID;
                                    _AccountRole.AccountName = _Account.UserName;
                                    _AccountRole.RoleID = Convert.ToInt32(_strSplit[0]);
                                    _AccountRole.RoleName = _strSplit[1];
                                    _IAccountRoleImpl.Add(_AccountRole);
                                }
                            }
                            //#########################################

                            return RedirectToAction("Add");
                        }

                    }
                }
                catch (Exception ex)
                {
                    TempData["MessageError"] = ex.InnerException;
                }


         
            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            HttpCookie cookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }
            return RedirectToAction("Index", "LoginForm");
        }

        [Authorize]
        public ActionResult AccountInfo()
        {
            try
            {
                var ticketData = ((FormsIdentity)HttpContext.User.Identity).Ticket.UserData;
                WrapUserData _WrapUserData = new JavaScriptSerializer().Deserialize<WrapUserData>(ticketData);
                var _account = _IAccountImpl.GetById(_WrapUserData.AccountID);
                ViewBag.AccountRoles = _IAccountRoleImpl.GetByAccountID(_WrapUserData.AccountID).ToList();
                ViewBag.Roles = _IRoleImpl.All();

                if (_account == null)
                {
                    TempData["MessageError"] = "Không tồn tại thành viên";
                }
                else
                {
                    return View(_account);
                }
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return View(new Account());
        }

        [HttpPost]
        public ActionResult AccountInfo(Account model)
        {
            try {
                Account _Account = _IAccountImpl.GetById(model.ID);
                _Account.Email = model.Email;
                _Account.FullName = model.FullName;
                _IAccountImpl.Update(_Account);
                TempData["MessageSuccess"] = "Thay đổi thông tin cá nhân thành công";
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }


            return RedirectToAction("AccountInfo", "Account");
        }

        [Authorize]
        public ActionResult ChangePassword() {
            try
            {
                var ticketData = ((FormsIdentity)HttpContext.User.Identity).Ticket.UserData;
                WrapUserData _WrapUserData = new JavaScriptSerializer().Deserialize<WrapUserData>(ticketData);
                var _account = _IAccountImpl.GetById(_WrapUserData.AccountID);
                TempData["MessageInfo"] = "Khi thay đổi mật khẩu mới hợp lệ, hệ thống sẽ tự động thoát để bạn đăng nhập bằng mật khẩu bạn vừa thay đổi";
                if (_account == null)
                {
                    TempData["MessageError"] = "Không tồn tại thành viên";
                }
                else
                {
                    return View(_account);
                }
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return View(new Account());
        }

        [HttpPost]
        public ActionResult ChangePassword(Account model)
        {
            try
            {
                var _account = _IAccountImpl.GetById(model.ID);
                if (_account == null)
                {
                    TempData["MessageError"] = "Không tồn tại thành viên";
                }
                else
                {
                    _account.Password = AccountCrypto.Encrypt(model.Password, _account.UserName);
                    _IAccountImpl.Update(_account);
                    return RedirectToAction("Logout","Account");
                }
            }
            catch (Exception ex)
            {
                TempData["MessageError"] = ex.InnerException;
            }

            return View(new Account());
        }




    }
}
