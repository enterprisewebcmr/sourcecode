﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using Security.Foundation.Data;
using Security.Foundation.Data.Implements;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.WrapModels;

using System.Web.Security;

namespace BackOffices.CustomFilters
{
    public class ValidateUserRoles : FilterAttribute, IAuthorizationFilter
    {
        //private IAccountImpl _IAccountImpl;
        private IPageRoleImpl _IPageRoleImpl;
        private IPageImpl _IPageImpl;
        public ValidateUserRoles()
        {
            _IPageRoleImpl = new PageRoleImpl(new DatabaseFactory());
            _IPageImpl = new PageImpl(new DatabaseFactory());
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            try
            {
                var request = filterContext.HttpContext.Request;
                var response = filterContext.HttpContext.Response;
                if (request.IsAuthenticated)
                {


                    var ticketData = ((FormsIdentity)filterContext.HttpContext.User.Identity).Ticket.UserData;
                    WrapUserData _WrapUserData = new JavaScriptSerializer().Deserialize<WrapUserData>(ticketData);
                    string username = filterContext.HttpContext.User.Identity.Name;
                    if (!_WrapUserData.SupperAdmin)
                    {
                        string url = string.Format("/{0}/{1}", HttpContext.Current.Request.RequestContext.RouteData.Values["controller"], HttpContext.Current.Request.RequestContext.RouteData.Values["action"]);
                        if (HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower() == "index")
                        {
                            url = url.Replace("/Index", "");
                        }
                        var _page = _IPageImpl.GetByPageURL(url);
                        if (_page.ID > 0)
                        {

                            if (!_IPageRoleImpl.AllowAccessPage(_page.ID, _WrapUserData.RoleIDs))
                            {
                                ViewResult result = new ViewResult();
                                result.ViewName = "UnAccessable";
                                result.TempData["MessageError"] = "Bạn không có quyền truy cập link \"" + url + "\" tìm admin để được hỗ trợ tốt nhất";
                                filterContext.Result = result;
                                //filterContext.HttpContext.Response.Redirect("/");
                            }
                        }
                    }

                }
                else
                {
                    ViewResult result = new ViewResult();
                    result.ViewName = "UnAccessable";
                    result.TempData["MessageError"] = "Bạn chưa đăng nhập.";
                    filterContext.Result = result;
                    //filterContext.HttpContext.Response.Redirect("/");
                }
            }
            catch (Exception ex)
            {
                ViewResult result = new ViewResult();
                result.ViewName = "UnAccessable";
                result.TempData["MessageError"] = "Bạn không có quyền truy cập link tìm admin để được hỗ trợ tốt nhất";
                filterContext.Result = result;
            }

        }
    }
}