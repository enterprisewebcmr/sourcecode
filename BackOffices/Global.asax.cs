﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace BackOffices
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : NinjectHttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            ModelMetadataProviders.Current = new DataAnnotationsModelMetadataProvider();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            //RouteTable.Routes.MapHubs();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            GlobalFilters.Filters.Add(new HandleErrorAttribute());

            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory(Kernel));
        }


        protected override IKernel CreateKernel()
        {
            var modules = new INinjectModule[]
            {
                new ServiceModule()
            };

            return new StandardKernel(modules);
        }
    }

    #region longhk add more

    internal class ServiceModule : NinjectModule
    {
        public override void Load()
        {



            //#################### Security ###############################

            Bind<Security.Foundation.Data.Interfaces.IAccountImpl>().To<Security.Foundation.Data.Implements.AccountImpl>();
            Bind<Security.Foundation.Data.Interfaces.IRoleImpl>().To<Security.Foundation.Data.Implements.RoleImpl>();
            Bind<Security.Foundation.Data.Interfaces.IPageImpl>().To<Security.Foundation.Data.Implements.PageImpl>();
            Bind<Security.Foundation.Data.Interfaces.IPageRoleImpl>().To<Security.Foundation.Data.Implements.PageRoleImpl>();
            Bind<Security.Foundation.Data.Interfaces.IMenuImpl>().To<Security.Foundation.Data.Implements.MenuImpl>();
            Bind<Security.Foundation.Data.Interfaces.IMenuPageImpl>().To<Security.Foundation.Data.Implements.MenuPageImpl>();
            Bind<Security.Foundation.Data.Interfaces.IAccountRoleImpl>().To<Security.Foundation.Data.Implements.AccountRoleImpl>();
            Bind<Security.Foundation.Data.Interfaces.IPagePermissionImpl>().To<Security.Foundation.Data.Implements.PagePermissionImpl>();
            Bind<Security.Foundation.Data.Interfaces.IPagePermissionRoleImpl>().To<Security.Foundation.Data.Implements.PagePermissionRoleImpl>();

            Bind<Security.Foundation.Data.Interfaces.IDatabaseFactory>().To<Security.Foundation.Data.DatabaseFactory>();
            Bind<IDisposable>().To<Security.Foundation.Data.Disposable>();

            //######################################################################







        }
    }
    #endregion
}