﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffices.Models
{
    public class Report
    {
        public String ReportPartName { get; set; }
        public float Percentage { get; set;}
        public int count { get; set; }
    }
}