﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackOffices.Models
{
   public class Faculties
    {
        public int ID { get; set; }

        public string FaName { get; set; }

        public int? DLT_ID { get; set; }
        public string DLTName { get; set; }
        public DateTime? Create_Time { get; set; }
    }
}
