﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffices.Models
{
    public class Transcripts
    {
        public int Tran_ID { get; set; }
        public int OC_ID { get; set; }
        public string OC_Name { get; set; }
        public int? Student_ID { get; set; }
        public string Student_Name { get; set; }
        public int?  Mark { get; set; }
    }
}