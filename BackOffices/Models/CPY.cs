﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffices.Models
{
    public class CPY
    {

        public int ID { get; set; }

        public int CourseID { get; set; }

        public string CourseName { get; set; }

        public string Year { get; set; }

        public int CL_ID { get; set; }

        public string CL_Name { get; set; }

        public int CM_ID { get; set; }
        public string CM_Name { get; set; }

        public int? Status { get; set; }

        public DateTime Create_Time { get; set; }

        public string CPY_Code { get; set; }
    }
}