﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffices.Models
{
    public class OutComes
    {
        public int ID { get; set; }

        public int CPY_ID { get; set; }

        public string CPY_Name { get; set; }

        public string Year { get; set; }

        public string CPY_Code { get; set; }
        public string CL_Name { get; set; }
        public string OutCome_Name { get; set; }

        public int Percent { get; set; }

        public Dictionary<string, string> _DistributionGrade = new Dictionary<string, string>();
        public Dictionary<string, string> DistributionGrade
        {
            get {
                return _DistributionGrade;
            }
            set
            {
                _DistributionGrade = value;
            }

        }



    }
}