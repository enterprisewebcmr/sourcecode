﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffices.Models
{
    public class CMRs
    {
        public int CopyID { get; set; }

        public string FieldName { get; set; }

        public string FieldDes { get; set; }

        public string CLComment { get; set; }

        public string DLTComment { get; set; }
    }
}