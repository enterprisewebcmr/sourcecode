﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffices.Models
{
    public class Students
    {
        public int Student_ID { get; set; }
        public string StudentName { get; set; }
        public string DOB { get; set; }
        public string Phone_No { get; set; }
        public string Address { get; set; }
        public int? Status { get; set; }
        public DateTime Create_Time { get; set; }
        public string Email { get; set; }

        public string Mark { get; set; }

    }
}