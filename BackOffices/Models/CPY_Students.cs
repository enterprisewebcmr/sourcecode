﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffices.Models
{
    public class CPY_Students
    {
        public int CPY_Stdent_ID { get; set; }
        public int? CPY_ID { get; set; }

        public int Student_ID { get; set; }

        public string CPY_Name { get; set; }

        public string CPY_Code { get; set; }
        public string Year { get; set; }
    }
}