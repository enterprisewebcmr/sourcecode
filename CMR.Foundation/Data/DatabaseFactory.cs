﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMR.Foundation.Data.Interfaces;
using System.Configuration;

namespace CMR.Foundation.Data
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private CMRDatabase db;
        public CMRDatabase Get()
        {

            return db ?? (db = new CMRDatabase(ConfigurationManager.ConnectionStrings["CMRConnectionString"].ConnectionString));
        }

        protected override void DisposeCore()
        {
            if (db != null)
                db.Dispose();
        }
    }

    public class Disposable : IDisposable
    {
        private bool isDisposed;

        ~Disposable()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                DisposeCore();
            }

            isDisposed = true;
        }
        protected virtual void DisposeCore()
        {
        }
    }
}
