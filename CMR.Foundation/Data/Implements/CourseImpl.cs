﻿using CMR.Foundation.Data.Interfaces;
using CMR.Foundation.Data.Models;
using Security.Foundation.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMR.Foundation.Data.Implements
{
    public class CourseImpl : RepositoryBase<Course>, ICourseImpl
    {
        public CourseImpl(IDatabaseFactory DbFactory) : base(DbFactory) { }
    }
}
