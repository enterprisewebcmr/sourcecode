﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMR.Foundation.Data.Models
{
    class Faculties
    {
        public int ID { get; set; }

        public string FaName { get; set; }

        public int DLT_ID { get; set; }
    }
}
