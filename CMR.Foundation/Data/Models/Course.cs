﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMR.Foundation.Data.Models
{
    public class Course
    {
        public int ID { get; set; }
        public string CourseName { get; set; }
        public string CourseStatus { get; set; }
        
        public int Fa_ID { get; set; }
    }
}
