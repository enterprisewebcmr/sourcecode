﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Interfaces;
using System.Configuration;

namespace Security.Foundation.Data
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private CMRSecurityDB db;
        public CMRSecurityDB Get()
        {

            return db ?? (db = new CMRSecurityDB(ConfigurationManager.ConnectionStrings["CMRSecurityConnectionString"].ConnectionString));
        }

        protected override void DisposeCore()
        {
            if (db != null)
                db.Dispose();
        }
    }

    public class Disposable : IDisposable
    {
        private bool isDisposed;

        ~Disposable()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                DisposeCore();
            }

            isDisposed = true;
        }
        protected virtual void DisposeCore()
        {
        }
    }
}
