﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;


namespace Security.Foundation.Data.Interfaces
{
    public interface IAccountRoleImpl: IRepository<AccountRole>
    {
        void DeleteAllByAccountID(int AccountID);
        IEnumerable<AccountRole> GetByAccountID(int Accountid);
        IEnumerable<AccountRole> GetByRoleID(int RoleID);
    }
}
