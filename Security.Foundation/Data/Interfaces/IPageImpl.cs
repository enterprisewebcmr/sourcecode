﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Interfaces
{
    public interface IPageImpl: IRepository<Page>
    {
        IEnumerable<Page> GetByParentID(int parentid);
        Page GetByPageURL(string pageurl);
    }
}
