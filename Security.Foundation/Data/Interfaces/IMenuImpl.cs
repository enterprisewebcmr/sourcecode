﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Interfaces
{
    public interface IMenuImpl: IRepository<Menu>
    {
        
    }
}
