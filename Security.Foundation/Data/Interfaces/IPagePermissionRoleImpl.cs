﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Interfaces
{
    public interface IPagePermissionRoleImpl: IRepository<PagePermissionRole>
    {
        void DeleteAll(int pperroleid);
        IEnumerable<PagePermissionRole> GetByPagePermissionID(int id);
        bool CheckAccess(string fname, int pageid, int[] roleids);

        IEnumerable<PagePermissionRole> GetByRoleID(int RoleID);
        IEnumerable<PagePermissionRole> GetByPagePermissionByPageID(int pageid);
        void DeleteAllByPageID(int pageid);


    }
}
