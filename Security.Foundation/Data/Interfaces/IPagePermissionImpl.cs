﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Interfaces
{
    public interface IPagePermissionImpl: IRepository<PagePermission>
    {
        IEnumerable<PagePermission> GetPermissionByPageID(int pageid);
        PagePermission Get(int pageid, string fname);
        void DeleteAllPagePermissionByPageID(int pageid);
    }
}
