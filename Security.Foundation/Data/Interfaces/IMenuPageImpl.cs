﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Interfaces
{
    public interface IMenuPageImpl: IRepository<MenuPage>
    {
        IEnumerable<MenuPage> GetMenuPageByPageID(int pageid);
        IEnumerable<Page> GetPageByMenuID(int menuid);
        void DeleteAllByPageID(int pageid);
        void DeleteAllByMenuID(int menuid);
        IQueryable<IGrouping<string, MenuPage>> GetMenuPageByRoleID(int[] roleid);
        IQueryable<IGrouping<string, MenuPage>> GetAllMenuPage();

        IEnumerable<MenuPage> GetMenuPageByMenuID(int menuid);

        
    }
}
