﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Interfaces
{
    public interface IPageRoleImpl: IRepository<PageRole>
    {
        void DeleteAllByPageID(int pageid);
        IEnumerable<PageRole> GetByPageID(int pageid);
        bool AllowAccessPage(int pageid, int[] roleids);

        IEnumerable<PageRole> GetbyRoleID(int RoleID);
    }
}
