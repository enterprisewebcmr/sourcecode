﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Interfaces
{
    public interface IDatabaseFactory
    {
        CMRSecurityDB Get();
    }
}
