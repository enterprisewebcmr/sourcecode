﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Implements
{
    public class PagePermissionImpl: RepositoryBase<PagePermission>, IPagePermissionImpl
    {
        public PagePermissionImpl(IDatabaseFactory DbFactory) : base(DbFactory) { }

        public IEnumerable<PagePermission> GetPermissionByPageID(int pageid) 
        {
            return this.HybridTVSecurityDB.PagePermissions.Where(pp=>pp.PageID == pageid);
        }

        public void DeleteAllPagePermissionByPageID(int pageid) {
            var results = GetPermissionByPageID(pageid).ToList();
            foreach (var item in results) {
                this.Delete(item);
            }
        }



        public PagePermission Get(int pageid, string fname) {
            return this.HybridTVSecurityDB.PagePermissions.FirstOrDefault(pp => pp.PageID == pageid && pp.FuncName == fname);
        }
    }
}
