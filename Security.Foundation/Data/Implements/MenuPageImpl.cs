﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Security.Foundation.Data.Models;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data;

namespace Security.Foundation.Data.Implements
{
    public class MenuPageImpl: RepositoryBase<MenuPage>, IMenuPageImpl
    {
        public MenuPageImpl(IDatabaseFactory DbFactory) : base(DbFactory) { }

        public IEnumerable<MenuPage> GetMenuPageByPageID(int pageid) {
            return this.HybridTVSecurityDB.MenuPages.Where(mp => mp.PageID == pageid);
        }

        public IEnumerable<Page> GetPageByMenuID(int menuid)
        {
            var results = from mp in HybridTVSecurityDB.MenuPages
                          from p in HybridTVSecurityDB.Pages
                          where mp.MenuID == menuid && p.ID == mp.PageID
                          select p;

            return results;
        }

        public IEnumerable<MenuPage> GetMenuPageByMenuID(int menuid) {
            var results = from mp in HybridTVSecurityDB.MenuPages
                          where mp.MenuID == menuid
                          select mp;

            return results;
        }

        public void DeleteAllByPageID(int pageid){
            var results = GetMenuPageByPageID(pageid).ToList();
            foreach (var item in results) {
                this.Delete(item);
            }
        }

        public void DeleteAllByMenuID(int menuid)
        {
            var results = GetMenuPageByMenuID(menuid).ToList();
            foreach (var item in results)
            {
                this.Delete(item);
            }
        }

        public IQueryable<IGrouping<string, MenuPage>> GetMenuPageByRoleID(int[] roleid)
        {
            var results = from pr in HybridTVSecurityDB.PageRoles
                          from mp in HybridTVSecurityDB.MenuPages
                          where roleid.Contains(pr.RoleID)
                          && pr.PageID == mp.PageID
                          group mp by mp.MenuName into g
                          select g;
            return results;
        }

        public IQueryable<IGrouping<string, MenuPage>> GetAllMenuPage() {
            var results = from pr in HybridTVSecurityDB.PageRoles
                          from mp in HybridTVSecurityDB.MenuPages
                          where pr.PageID == mp.PageID
                          group mp by mp.MenuName into g
                          select g;
            return results;
        }
    }
}
