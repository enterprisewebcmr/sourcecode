﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Security.Foundation.Data;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.Interfaces;

namespace Security.Foundation.Data.Implements
{
    public class PageImpl: RepositoryBase<Page>, IPageImpl
    {
        public PageImpl(DatabaseFactory DbFactoty) : base(DbFactoty) { }

        public IEnumerable<Page> GetByParentID(int parentid) {
            return this.HybridTVSecurityDB.Pages.Where(p => p.ParentPageID == parentid);
        }

        public Page GetByPageURL(string pageurl) {
            return this.HybridTVSecurityDB.Pages.FirstOrDefault(p => p.PageURL == pageurl);
        }
    }
}
