﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Security.Foundation.Data.Models;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data;

namespace Security.Foundation.Data.Implements
{
    public class AccountRoleImpl: RepositoryBase<AccountRole>, IAccountRoleImpl 
    {
        public AccountRoleImpl(IDatabaseFactory DbFactory) : base(DbFactory) { }

        public void DeleteAllByAccountID(int AccountID) {
            var results = this.HybridTVSecurityDB.AccountRoles.Where(ar => ar.AccountID == AccountID).ToList();
            foreach (var item in results) {
                this.Delete(item);
            }
        }

        public IEnumerable<AccountRole> GetByAccountID(int Accountid) {
            return this.HybridTVSecurityDB.AccountRoles.Where(ar => ar.AccountID == Accountid);
        }

        public IEnumerable<AccountRole> GetByRoleID(int RoleID) {
            var results = from ar in this.HybridTVSecurityDB.AccountRoles
                          where ar.RoleID == RoleID
                          select ar;

            return results;
        }
    }
}
