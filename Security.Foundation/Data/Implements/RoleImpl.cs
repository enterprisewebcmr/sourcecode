﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;
using Security.Foundation.Data;

namespace Security.Foundation.Data.Implements
{
    public class RoleImpl : RepositoryBase<Role>, IRoleImpl
    {
        public RoleImpl(IDatabaseFactory DbFactory) : base(DbFactory) { }
    }
}
