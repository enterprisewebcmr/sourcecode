﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data;
using Security.Foundation.Data.Interfaces;
using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.Implements
{
    public class PageRoleImpl: RepositoryBase<PageRole>, IPageRoleImpl
    {
        public PageRoleImpl(IDatabaseFactory DbFactory) : base(DbFactory) { }

        public void DeleteAllByPageID(int pageid) {
            var results = this.HybridTVSecurityDB.PageRoles.Where(pr => pr.PageID == pageid).ToList();

            foreach(var item in results){
                this.Delete(item);
            }
        }

        public IEnumerable<PageRole> GetByPageID(int pageid) {
            return this.HybridTVSecurityDB.PageRoles.Where(pr => pr.PageID == pageid);
        }

        public bool AllowAccessPage(int pageid, int[] roleids) {
            var results = from pr in HybridTVSecurityDB.PageRoles
                          where pr.PageID == pageid
                          && roleids.Contains(pr.RoleID)
                          select pr.PageID;

            return results.ToList().Count > 0;

        }

        public IEnumerable<PageRole> GetbyRoleID(int RoleID) {
            var results = from pr in this.HybridTVSecurityDB.PageRoles
                          where pr.RoleID == RoleID
                          select pr;

            return results;
        }
    }
}
