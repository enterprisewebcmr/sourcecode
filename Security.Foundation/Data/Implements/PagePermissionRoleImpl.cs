﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data;
using Security.Foundation.Data.Models;
using Security.Foundation.Data.Interfaces;


namespace Security.Foundation.Data.Implements
{
    public class PagePermissionRoleImpl: RepositoryBase<PagePermissionRole>, IPagePermissionRoleImpl
    {
        public PagePermissionRoleImpl(IDatabaseFactory DbFactory) : base(DbFactory) { }

        public void DeleteAll(int pperroleid) {
            var results = this.HybridTVSecurityDB.PagePermissionRoles.Where(ppr => ppr.PagePermissionID == pperroleid).ToList();
            foreach (var item in results) {
                this.Delete(item);
            }
        }

        public IEnumerable<PagePermissionRole> GetByPagePermissionID(int id) {
            return this.HybridTVSecurityDB.PagePermissionRoles.Where(ppr => ppr.PagePermissionID == id);
        }

        public IEnumerable<PagePermissionRole> GetByPagePermissionByPageID(int pageid)
        {
            return this.HybridTVSecurityDB.PagePermissionRoles.Where(ppr => ppr.PageID == pageid);
        }

        public void DeleteAllByPageID(int pageid)
        {
            var results = GetByPagePermissionByPageID(pageid).ToList();
            foreach (var item in results)
            {
                this.Delete(item);
            }
        }


        public bool CheckAccess(string fname, int pageid, int[] roleids) {

            var results = from ppr in HybridTVSecurityDB.PagePermissionRoles
                          where ppr.FuncName == fname
                          && ppr.PageID == pageid
                          && roleids.Contains(ppr.RoleID)
                          select ppr;

            return results.ToList().Count > 0;
        }

        public IEnumerable<PagePermissionRole> GetByRoleID(int RoleID) {

            var results = from ppr in HybridTVSecurityDB.PagePermissionRoles
                          where ppr.RoleID == RoleID
                          select ppr;

            return results;

        }
    }
}
