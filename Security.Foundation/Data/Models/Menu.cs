﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class Menu
    {
        public int ID { get; set; }
        public string MenuName { get; set; }
        public string CssClass { get; set; }
    }
}
