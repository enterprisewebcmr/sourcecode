﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class MenuPage
    {
        public int ID { get; set; }
        public string MenuName { get; set; }
        public int MenuID { get; set; }
        public string PageName { get; set; }
        public string PageURL { get; set; }
        public int PageID { get; set; }
    }
}
