﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class Page
    {
        public int ID { get; set; }
        public string PageName { get; set; }
        public string PageURL { get; set; }
        public int ParentPageID { get; set; }
    }
}
