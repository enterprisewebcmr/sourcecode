﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class Account
    {
        public Account() { }
        public int ID { get; set; }

        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Locked { get; set; }
        public bool SupperAdmin { get; set; }
    }
}
