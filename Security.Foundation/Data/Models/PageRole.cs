﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class PageRole
    {
        public int ID { get; set; }
        public string PageName { get; set; }
        public int PageID { get; set; }
        public string RoleName { get; set; }
        public int RoleID { get; set; }
    }
}
