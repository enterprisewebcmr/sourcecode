﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class PagePermission
    {
        public int ID { get; set; }
        public int PageID { get; set; }
        public string PageURL { get; set; }
        public string FuncName { get; set; }
        public string Description { get; set; }
    }
}
