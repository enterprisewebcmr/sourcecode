﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class AccountRole
    {
        public int ID { get; set; }
        public string AccountName { get; set; }
        public int AccountID { get; set; }
        public string RoleName { get; set; }
        public int RoleID { get; set; }
    }
}
