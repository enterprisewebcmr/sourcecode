﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.Models
{
    public class PagePermissionRole
    {
        public int ID { get; set; }
        public string FuncName { get; set; }
        public int PageID { get; set; }
        public int PagePermissionID { get; set; }
        public string RoleName { get; set; }
        public int RoleID { get; set; }
    }
}
