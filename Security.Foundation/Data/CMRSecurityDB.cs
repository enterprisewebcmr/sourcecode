﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using Security.Foundation.Data.Models;

namespace Security.Foundation.Data
{
    public class CMRSecurityDB: DbContext
    {
        public CMRSecurityDB(string connectionString) : base(connectionString) { }


        public DbSet<Account> Accounts { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<PageRole> PageRoles { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuPage> MenuPages { get; set; }
        public DbSet<AccountRole> AccountRoles { get; set; }
        public DbSet<PagePermission> PagePermissions { get; set; }
        public DbSet<PagePermissionRole> PagePermissionRoles { get; set; }


        public virtual IDbSet<T> DbSet<T>() where T : class
        {
            return Set<T>();
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
