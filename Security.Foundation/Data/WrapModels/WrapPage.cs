﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.WrapModels
{
    public class WrapPage
    {
        public Page Page { get; set; }
        public List<Page> SubPages { get; set; }
    }
}
