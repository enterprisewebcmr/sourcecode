﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.WrapModels
{
    public class WrapMenuPage
    {
        public string MenuName { get; set; }
        public List<MenuPage> MenuPages { get; set; }
    }
}
