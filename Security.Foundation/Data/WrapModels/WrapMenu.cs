﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Security.Foundation.Data.Models;

namespace Security.Foundation.Data.WrapModels
{
    public class WrapMenu
    {
        public Menu Menu { get; set; }
        public IEnumerable<Page> Pages{get;set;}


    }
}
