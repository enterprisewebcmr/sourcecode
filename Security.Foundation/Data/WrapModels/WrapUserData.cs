﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Foundation.Data.WrapModels
{
    public class WrapUserData
    {
        public int[] RoleIDs {get;set;}
        public bool SupperAdmin { get; set; }

        public string Username { get; set; }
        public int AccountID { get; set; }
    }
}
